const dev_server = 'http://139.59.93.89/';

/**
* Base URL for all the APIs
*/
const BaseUrl = dev_server;

/**
* API Constanst for different actions which are used to call an particular API
*/
// export const BASE_URL                =           BaseUrl+':3000/'
export const LOGIN_API              		=           BaseUrl+'rest-auth/login/'
export const AAHDAAR_API            		=           BaseUrl+'authentication/mobile/aadhaar-call/'
export const SENDOTP_API            		=           BaseUrl+'authentication/mobile/send-email-otp/'
export const VALIDATE_OTP_API            	=           BaseUrl+'authentication/web/validate-email-otp/'
export const CREATE_ARTICLE_API     		=			BaseUrl+'article/api/v1/'

