/*common types*/
export const HAS_ERROR = 'HAS_ERROR';
export const UPLOAD_PROGRESS = 'UPLOAD_PROGRESS';

/*Auth types*/
export const GET_LOGIN_DATA = 'GET_LOGIN_DATA'
export const GET_AAHDAAR_DATA = 'GET_AAHDAAR_DATA'
export const GET_OTP_DATA = 'GET_OTP_DATA'
export const GET_VALIDATE_OTP_DATA = 'GET_CONFIRM_OTP_DATA'



/*article types*/
export const CREATE_ARTICLE_DATA = 'CREATE_ARTICLE_DATA'
