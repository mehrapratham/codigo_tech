import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Splash from './containers/Splash';
import StartScreen from './containers/StartScreen';
import BuildScreen from './containers/StartScreen/BuildScreen';
import Recognized from './containers/StartScreen/Recognized';
import Login from './containers/Auth/login';
import AadharCardScreen from './containers/GetStarted/aadhar'
import ReceiveOtp from './containers/GetStarted/ReceiveOtp'
import SelectProfile from './containers/GetStarted/selectProfile'
import resetPassword from './containers/GetStarted/resetPassword'
import SelectCourse from './containers/GetStarted/selectCourse'
import securityCode from './containers/GetStarted/securityCode'
import SelectSkills from './containers/GetStarted/selectSkills'
import SelectCollege from './containers/GetStarted/selectCollege'
import SelectCompany from './containers/GetStarted/selectCompany'
import ForgotPassword from './containers/ForgotPassword/ForgotPassword'
import ResetPassword from './containers/ForgotPassword/ResetPassword'
import SecurityCode from './containers/ForgotPassword/SecurityCode'
import Feed from './containers/Feed'
import Likes from './containers/Feed/Likes'
import FeedScreen from './containers/Feed/Feed'
import ArticleFirstStep from './containers/Article'
import ArticleForm from './containers/Article/ArticleForm'
import ArticleDetail from './containers/Article/ArticleDetail'
import FeedDetail from './containers/Feed/FeedDetail'
import ArticleList from './containers/Article/ArticleList'
import RecentSearch from './containers/Search/RecentSearch'
import SearchResult from './containers/Search/SearchResult'
import Research from './containers/Article/Research'
import UploadIdCard from './containers/GetStarted/UploadIdCard'
import ThankYouScreen from './containers/GetStarted/ThankYouScreen'
import UploadIdDetail from './containers/GetStarted/UploadIdDetail'
import EventScreen from './containers/EventStudent'
import EventList from './containers/EventStudent/Events'
import EventForm from './containers/EventFaculty'
import StudentProfile from './containers/EventStudent/StudentProfile';
import CongratulationScreen from './containers/EventStudent/CongratulationScreen'
import CollegeEvent from './containers/EventStudent/CollegeEvent'
import AddNewCardScreen from './containers/EventStudent/AddNewCardScreen'
import AddMedia from './containers/EventFaculty/AddMedia';
import Dashboard from './containers/EventFaculty/Dashboard';
import AddMembers from './containers/EventFaculty/AddMembers'
import AddQuestions from './containers/EventFaculty/AddQuestions'
import EventSuccess from './containers/EventFaculty/EventSuccess'
import Events from './containers/EventFaculty/Events';
import RequestReview from './containers/EventFaculty/RequestReview';
export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    return (
      <Router hideNavBar= "true">
        <Scene key="root" duration={0}>
          <Scene key="Splash" component={Splash} hideNavBar title="PageOne" initial={true} />
          <Scene key="StartScreen" component={StartScreen} hideNavBar />
          <Scene key="BuildScreen" component={BuildScreen} hideNavBar  />
          <Scene key="Recognized" component={Recognized} hideNavBar title="Recognized" />
          <Scene key="Login" component={Login} title="Login" hideNavBar initial={true} />
          <Scene key="AadharCardScreen" component={AadharCardScreen} title="Get Started" hideNavBar />
          <Scene key="ReceiveOtp" component={ReceiveOtp} title="Receive OTP" hideNavBar />
          <Scene key="SelectProfile" component={SelectProfile} title="Select Profile" hideNavBar />
          <Scene key="resetPassword" component={resetPassword} title="reset Password" hideNavBar />
          <Scene key="SelectCourse" component={SelectCourse} title="Select Course" hideNavBar />
          <Scene key="SelectSkills" component={SelectSkills} title="Select Skills" hideNavBar />
          <Scene key="SelectCollege" component={SelectCollege} title="Select College" hideNavBar />
          <Scene key="SelectCompany" component={SelectCompany} title="Select Company" hideNavBar />
          <Scene key="securityCode" component={securityCode} title="security Code" hideNavBar />
          <Scene key="ForgotPassword" component={ForgotPassword} title="Forgot Password" hideNavBar />
          <Scene key="SecurityCode" component={SecurityCode} title="Security Code" hideNavBar />
          <Scene key="Feed" component={Feed} title="Feed" hideNavBar />
          <Scene key="Likes" component={Likes} title="Likes" hideNavBar />
          <Scene key="Feed" component={Feed} title="Feed" hideNavBar />
          <Scene key="Likes" component={Likes} title="Likes" hideNavBar />
          <Scene key="FeedScreen" component={FeedScreen} title="Feed Screen" hideNavBar />
          <Scene key="ArticleFirstStep" component={ArticleFirstStep} title="Article First Step" hideNavBar  />
          <Scene key="ArticleForm" component={ArticleForm} title="Article Form" hideNavBar  />
          <Scene key="ArticleDetail" component={ArticleDetail} title="Article Detail" hideNavBar />
          <Scene key="FeedDetail" component={FeedDetail} title="Feed Detail" hideNavBar />
          <Scene key="ArticleList" component={ArticleList} title="Article List" hideNavBar />
          <Scene key="ResetPassword" component={ResetPassword} title="Reset Password" hideNavBar />
          <Scene key="RecentSearch" component={RecentSearch} title="Recent Search" hideNavBar />
          <Scene key="SearchResult" component={SearchResult} title="Search Result" hideNavBar />
          <Scene key="Research" component={Research} title="Reset Password" hideNavBar />
          <Scene key="UploadIdCard" component={UploadIdCard} title="Upload Id Card" hideNavBar />
          <Scene key="ThankYouScreen" component={ThankYouScreen} title="Thank You Screen" hideNavBar />
          <Scene key="UploadIdDetail" component={UploadIdDetail} title="Upload Id Detail" hideNavBar />
          <Scene key="EventScreen" component={EventScreen} title="Event Screen" hideNavBar />
          <Scene key="EventForm" component={EventForm} title="Event Form" hideNavBar />
          <Scene key="StudentProfile" component={StudentProfile} title="StudentProfile" hideNavBar />
          <Scene key="CongratulationScreen" component={CongratulationScreen} title="CongratulationScreen" hideNavBar  />
          <Scene key="EventList" component={EventList} title="EventList" hideNavBar  />
          <Scene key="CollegeEvent" component={CollegeEvent} title="CollegeEvent" hideNavBar />
          <Scene key="AddNewCardScreen" component={AddNewCardScreen} title="AddNewCardScreen" hideNavBar  />
          <Scene key="Dashboard" component={Dashboard} title="Dashboard" hideNavBar  />
          <Scene key="AddMedia" component={AddMedia} title="Add Media" hideNavBar />
          <Scene key="AddMembers" component={AddMembers} title="Add Members" hideNavBar />
          <Scene key="AddQuestions" component={AddQuestions} title="Add Questions" hideNavBar />
          <Scene key="EventSuccess" component={EventSuccess} title="Event Success" hideNavBar />
          <Scene key="Events" component={Events} title="Events" hideNavBar />
          <Scene key="RequestReview" component={RequestReview} title="RequestReview" hideNavBar />
        </Scene>
      </Router>
    )
  }
}