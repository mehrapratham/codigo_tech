import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import { Container, Tabs, Tab } from 'native-base';
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import SearchPost from '../../components/Search/SearchPost'
import SearchMember from '../../components/Search/SearchMember'

export default class SearchResult extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			
		}
	}
	render(){
		return(
			<Container style={{paddingTop: 24}}>
        <View style={[styles.hasFlex]}>
          <View style={[styles.backGradient, {backgroundColor: '#e4e5e7'}]}>
            <FeedSearchBox placeholder="Search" arrowBack={true} />
          </View>
          <View style={styles.hasFlex}>
          	<Tabs tabBarUnderlineStyle={styles.tabBarUnderline}>
	            <Tab heading="ALL" tabStyle={styles.customGrayTabStyle} activeTabStyle={styles.customGrayTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
	              <ScrollView>
	              	<View style={styles.searchFoundCon}>
	              		<Text style={styles.searchFound}>You Found </Text>
	              		<Text style={styles.searchFoundBold}>3,212 </Text>
	              		<Text style={styles.searchFound}>results for </Text>
	              		<Text style={styles.searchFoundBold}>"Madhavi"</Text>
	              	</View>
	            		<SearchMember title={'Student'} rightText={'See All'}/>
	            		<SearchMember title={'Faculty'} rightText={'See All'}/>
	              	<SearchPost />
	              </ScrollView>
	            </Tab>
	            <Tab heading="POSTS" tabStyle={styles.customGrayTabStyle} activeTabStyle={styles.customGrayTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
	            	<ScrollView>
	              	<SearchPost />
	              </ScrollView>
	            </Tab>
	            <Tab heading="STUDENTS" tabStyle={styles.customGrayTabStyle} activeTabStyle={styles.customGrayTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
	            	<ScrollView>
	            		<SearchMember title={'Student'} rightText={'See All'}/>
	            	</ScrollView>
	            </Tab>
	            <Tab heading="FACULTY" tabStyle={styles.customGrayTabStyle} activeTabStyle={styles.customGrayTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
	            	<ScrollView>
	            		<SearchMember title={'Faculty'} rightText={'See All'}/>
	            	</ScrollView>
	            </Tab>
	          </Tabs>
          </View>
        </View>
      </Container>
		)
	}
}
