import React from 'react'
import { View, Text, TouchableOpacity, Image, Dimensions, TextInput, ScrollView } from 'react-native'
import TopHeader from '../../components/common/header'
import { Container, Content } from 'native-base';
import FeedUser from '../../components/common/FeedUser'
import styles from '../../styles/styles'
import FeedActions from '../../components/common/FeedActions'
import {Video} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import AddComment from '../../components/common/AddComment'
import MultiImages from '../../components/common/MultiImages'
import FollowCard from '../../components/common/FollowCard'
import JobCard from '../../components/common/JobCard'
import CommentSection from '../../components/common/CommentSection'
import FileInFeed from '../../components/common/FileInFeed'
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import { LinearGradient } from 'expo';
import { Actions } from 'react-native-router-flux'
import Popup from '../../components/common/Popup'

export default class Feed extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			universityData: [
				{
					title: "NIT Suratkal",
					subTitle: "Suratkal, Karnataka",
				},
				{
					title: "NIT Suratkal",
					subTitle: "Suratkal, Karnataka",
				},
				{
					title: "NIT Suratkal",
					subTitle: "Suratkal, Karnataka",
				}
			],
			peopleData: [
				{
					title: "Rupinderpal Singh",
					subTitle: "Suratkal, Karnataka",
					mutualCon: 30
				},
				{
					title: "Pratham Kumar",
					subTitle: "Suratkal, Karnataka",
					mutualCon: 30
				},
				{
					title: "Swati Kaur",
					subTitle: "Suratkal, Karnataka",
					mutualCon: 30
				}
			],
			companyData: [
				{
					title: "Microsoft",
					subTitle: "Suratkal, Karnataka"
				},
				{
					title: "Oracle",
					subTitle: "Suratkal, Karnataka"
				},
				{
					title: "Zestgeek",
					subTitle: "Suratkal, Karnataka"
				}
			],
			jobData: [
				{
					title: "Microsoft",
					subTitle: "Suratkal, Karnataka",
					alumni: 22
				},
				{
					title: "Oracle",
					subTitle: "Suratkal, Karnataka",
					alumni: 22
				},
				{
					title: "Zestgeek",
					subTitle: "Suratkal, Karnataka",
					alumni: 22
				}
			]
		}
	}
	componentWillMount(){

	}
	popupOpen() {
       this.popupDialog.show();
   	}
   	popupClose() {
       this.popupDialog.dismiss()
	}
	render(){
		const { height,width } = Dimensions.get('window');
		return(
			<Container>
				<View style={[styles.hasGrayBackground,styles.hasFlex]}>
		        <ScrollView>
					<View style={styles.feedCon}>
						<View style={styles.feedHeader}>
							<View style={styles.marginBottom20}>
								<FeedUser />
							</View>
							<View>
								<TouchableOpacity>
									<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy: </Text><Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
								</TouchableOpacity>
							</View>
						</View>
						<TouchableOpacity onPress={() =>{Actions.FeedDetail()}} activeOpacity={0.5}>
        					<Image source={require('../../img/feed.png')} style={styles.feedImg} />
						</TouchableOpacity>
						<View>
							<FeedActions onLikePress={() => {Actions.Likes()}}/>
						</View>
						<View>
							<AddComment />
						</View>
					</View>
					<View style={styles.feedCon}>
						<View style={[styles.jobCard, {width: '100%', borderWidth: 0, margin: 0}]}>
							<View style={styles.paddingTop10}>
								<FeedUser isSmall={true} size={45} sliderUpImage={6}/>
							</View>
							<View style={styles.allumniCon}>
								<Image source={require('../../img/allumni.png')} style={styles.allmniIcon} />
								<Text style={styles.allumniConText}>18 college allumni work here</Text>
							</View>
							<Text style={styles.borderBox}></Text>
							<TouchableOpacity style={styles.centered}>
								<Text style={styles.followLink}>View Job</Text>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.followCon}>
							<Text style={styles.mainText}>Jobs recommended for you</Text>
							<TouchableOpacity style={styles.sideLink}>
								<Text style={styles.sideLinkText}>See All</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.followOuterCon}>
							<ScrollView style={styles.followScrollBox} horizontal={true} showsHorizontalScrollIndicator={false}>
								{this.state.jobData.map((item, key) =>{
									return <JobCard data={item} key={key} width={230} />
								})}
							</ScrollView>
						</View>
					</View>
					
					<View style={styles.feedCon}>
						<View style={[styles.feedHeader]}>
							<View style={styles.marginBottom20}>
								<FeedUser	/>
							</View>
							<View>
								<TouchableOpacity>
									<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
							<FileInFeed />
						</View>
						<View>
							<FeedActions />
						</View>
						<View>
							<AddComment />
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.feedHeader}>
							<View>
								<FeedUser userAction={true}/>
							</View>
						</View>
						<View style={[styles.feedHeader, styles.smallUserCon]}>
							<View style={styles.marginBottom20}>
								<FeedUser isSmall={true} size={36}/>
							</View>
							<View>
								<TouchableOpacity>
									<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
        			<Image source={require('../../img/feed.png')} style={styles.feedImg} />
						</View>
						<View>
							<FeedActions />
						</View>
						<View>
							<AddComment />
						</View>
					</View>
					<View style={styles.feedCon}>
						<View style={styles.feedHeader}>
							<View style={styles.marginBottom20}>
								<FeedUser />
							</View>
							<View>
								<Text style={styles.videoText}>This Video explain fluid statics and dynamics in detail.</Text>
							</View>
						</View>
						<View>
		        			<VideoPlayer
				               	videoProps={{
				                 	shouldPlay: false,
				                 	resizeMode: Video.RESIZE_MODE_CONTAIN,
				                 	source: {
				                   	uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
				                 	},
				               	}}
				                isPortrait={true}
				                playFromPositionMillis={0}
				              />
						</View>
						<View>
							<FeedActions />
						</View>
						<View>
							<AddComment />
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.feedHeader}>
							<View style={styles.marginBottom20}>
								<FeedUser />
							</View>
							<View>
								<TouchableOpacity>
									<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
        				<MultiImages />
						</View>
						<View>
							<FeedActions />
						</View>
						<View>
							<AddComment />
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.followCon}>
							<Text style={styles.mainText}>People you may know</Text>
							<TouchableOpacity style={styles.sideLink}>
								<Text style={styles.sideLinkText}>See All</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.followOuterCon}>
							<ScrollView style={styles.followScrollBox} horizontal={true} showsHorizontalScrollIndicator={false}>
								{this.state.universityData.map((item, key)=>{
									return <FollowCard data={item} key={key} width={150} />
								})}
							</ScrollView>
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.followCon}>
							<Text style={styles.mainText}>Universities to Follow</Text>
							<TouchableOpacity style={styles.sideLink}>
								<Text style={styles.sideLinkText}>See All</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.followOuterCon}>
							<ScrollView style={styles.followScrollBox} horizontal={true} showsHorizontalScrollIndicator={false}>
								{this.state.peopleData.map((item, key)=>{
									return <FollowCard data={item} key={key} width={200} />
								})}
							</ScrollView>
						</View>
					</View>
					<View style={styles.feedCon}>
						<View style={styles.followCon}>
							<Text style={styles.mainText}>Companies to Follow</Text>
							<TouchableOpacity style={styles.sideLink}>
								<Text style={styles.sideLinkText}>See All</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.followOuterCon}>
							<ScrollView style={styles.followScrollBox} horizontal={true} showsHorizontalScrollIndicator={false}>
								{this.state.companyData.map((item, key)=>{
									return <FollowCard data={item} key={key} width={150} />
								})}
							</ScrollView>
						</View>
					</View>

					<View style={styles.feedCon}>
						<View style={styles.feedHeader}>
							<View style={styles.marginBottom20}>
								<FeedUser />
							</View>
							<View>
								<TouchableOpacity>
									<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
								</TouchableOpacity>
							</View>
						</View>
						<View>
        				<MultiImages />
						</View>
						<View>
							<FeedActions />
						</View>
						<View style={styles.commentsOuterBox}>
							<CommentSection />
							<CommentSection />
							<CommentSection />
						</View>
						<View>
							<AddComment />
						</View>
					</View>
				</ScrollView>
				
				</View>
				
			</Container>
		)
	}
}
