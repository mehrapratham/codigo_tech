import React, { Component } from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Icon } from 'native-base';
import styles from '../../styles/styles';
import ChatList from '../../components/common/ChatList'
import { LinearGradient } from 'expo'
export default class Chats extends Component {
  constructor(props){
    super(props);
    this.state = {
      list: [
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: true 
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        },
        {
          name: 'Marion Moore',
          text: 'Freelance Design tricks how ',
          time: '11m ago',
          msgUnSeen: false
        }
      ]
    }
  }
  render() {
    return (
      <Container>
        <Header style={styles.whiteColor}>
          <Left>
            <Text style={[styles.marginLeft20,styles.fontSize22,styles.x]}>Chats</Text>
          </Left>
          <Right>
            <TouchableOpacity style={styles.marginRight20} activeOpacity={0.5}><Icon name="search" style={[styles.fontSize22,styles.darkGreyColor]}/></TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5}><Icon name="md-more" style={[styles.fontSize22,styles.darkGreyColor,styles.marginRight10]}/></TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <View> 
            <List>
              {this.state.list.map((item,key) => {
                return(
                  <ChatList list={item} key={key} /> 
                  )
                })
              }
            </List>
          </View>
        </Content>
        <TouchableOpacity activeOpacity={0.5}>
          <LinearGradient
            colors={['#58c075', '#4cb9d7']}
            style={[styles.addGradientButtn,styles.fixedPosition]}
            start={[0.1, 0.4]} end={[1.3, 0.3]}
          >
            <Text style={[styles.colorWhite,styles.boldText,styles.fontSize26,styles.lessMargin5]}>+</Text>
          </LinearGradient>
        </TouchableOpacity>
      </Container>
    );
  }
}
