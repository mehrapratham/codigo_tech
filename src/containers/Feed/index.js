import React from 'react'
import { View, StyleSheet, Animated, TouchableOpacity, Image } from 'react-native'
import { Container, Content, Footer, FooterTab, Button, Icon, Text } from 'native-base';
import styles from '../../styles/styles'
import Notifications from './Notifications'
import Chats from './Chats'
import Home from './Home'
import Article from '../Article'
import Popup from '../../components/common/Popup'
import { Actions } from 'react-native-router-flux'
export default class Main extends React.Component {
	constructor() {
		super();
		this.state = {
			activeTabs: 1,
			addMenuOpacity: new Animated.Value(0),
			addMenuInnerOpacity: new Animated.Value(0),
			innerBottom: new Animated.Value(-10),
			isOpened: false
		}
	}
	componentDidMount(){
		this.popupDialog.show();
	}
	onChangeTabs(tabs) {
		const { activeTabs } = this.state;
		this.setState({ activeTabs: tabs })
		if (this.state.isOpened) {
			this.toggleAddMenu()
		}
	}

	toggleAddMenu(){
		if (this.state.isOpened) {
			this.setState({isOpened: false})
			Animated.timing(
	      this.state.addMenuOpacity,
	      {
	        toValue: 0,
	        duration: 500,
	      }
	    ).start();

	    Animated.timing(
	      this.state.innerBottom,
	      {
	        toValue: -10,
	        duration: 300,
	      }
	    ).start();

		}
		else{
			this.setState({isOpened: true})
			Animated.timing(
	      this.state.addMenuOpacity,
	      {
	        toValue: 1,
	        duration: 500,
	      }
	    ).start();

	    Animated.timing(
	      this.state.innerBottom,
	      {
	        toValue: 0,
	        duration: 300,
	      }
	    ).start();
		}
	}

	popupOpen() {
     this.popupDialog.show();
 	}
 	popupClose() {
       this.popupDialog.dismiss()
	}
	render() {
		return (
			<View style={{flex: 1}}>
				<View style={{flex: 1,zIndex: -1}}>
					{this.state.activeTabs == 1 && <Home />}
					{this.state.activeTabs == 2 && <Notifications />}
					{this.state.activeTabs == 3 && <Article />}
					{this.state.activeTabs == 4 && <Chats />}
					{this.state.activeTabs == 5 && <View style={{marginTop: 30}}>Comming soon</View>}
					<Popup link={(popupDialog) => { this.popupDialog = popupDialog; }}
	                   	onPress={this.popupClose.bind(this)} label='Congratulations!' description="You are all set to go"
	                   	btnText="DONE"
	                   	style={styles.transOrBorderRadius}
               		/>
				</View>
				{this.state.isOpened && <Animated.View style={[styles.addMenu, {opacity: this.state.addMenuOpacity}]}>
					<Animated.View style={[styles.innerAddMenu]}>
						<View style={styles.uploadHeadingCon}>
							<Text style={styles.uploadHeading}>Upload</Text>
						</View>
						<View>
							<TouchableOpacity style={styles.uploadLink}>
								<Image source={require('../../img/postIcon.png')} style={styles.uploadLinkImg} />
								<Text style={styles.uploadLinkText}>Post</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.uploadLink} onPress={() => {Actions.ArticleFirstStep()}}>
								<Image source={require('../../img/postIcon.png')} style={styles.uploadLinkImg} />
								<Text style={styles.uploadLinkText}>Portfolio</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.uploadLink} onPress={() => {Actions.EventForm()}}>
								<Image source={require('../../img/postIcon.png')} style={styles.uploadLinkImg} />
								<Text style={styles.uploadLinkText}>Event</Text>
							</TouchableOpacity>
						</View>
					</Animated.View>
				</Animated.View>}
				
				<Footer style={styles.whiteColor}>
					<FooterTab style={styles.whiteColor}>
						<Button active={this.state.activeTabs == 1 && !this.state.isOpened} style={styles.whiteColor} onPress={this.onChangeTabs.bind(this, 1)} >
							<Icon name="home" style={(this.state.activeTabs == 1 && !this.state.isOpened) ? styles.activeFooterText : styles.textFooterColor} />
							<Text style={(this.state.activeTabs == 1 && !this.state.isOpened) ? [styles.activeFooterText,styles.fontSize10] : [styles.textFooterColor,styles.fontSize10]}>Feeds</Text>
						</Button>
						<Button active={(this.state.activeTabs == 2 && !this.state.isOpened)} style={styles.whiteColor} onPress={this.onChangeTabs.bind(this, 2)}>
							<Icon name="ios-notifications" style={(this.state.activeTabs == 2 && !this.state.isOpened) ? styles.activeFooterText : styles.textFooterColor} />
							<Text style={(this.state.activeTabs == 2 && !this.state.isOpened) ? [styles.activeFooterText,styles.fontSize10] : [styles.textFooterColor,styles.fontSize10]}>Notify</Text>
						</Button>
						<Button active={this.state.isOpened} style={styles.whiteColor} onPress={this.toggleAddMenu.bind(this)}>
							<Icon name="md-add-circle" style={this.state.isOpened ? styles.activeFooterText : styles.textFooterColor} />
							<Text style={this.state.isOpened ? [styles.activeFooterText,styles.fontSize10] : [styles.textFooterColor,styles.fontSize10]}>Upload</Text>
						</Button>
						<Button active={(this.state.activeTabs == 4 && !this.state.isOpened)} style={styles.whiteColor} onPress={this.onChangeTabs.bind(this, 4)}>
							<Icon name="md-chatboxes" style={(this.state.activeTabs == 4 && !this.state.isOpened) ? styles.activeFooterText : styles.textFooterColor} />
							<Text style={(this.state.activeTabs == 4 && !this.state.isOpened) ? [styles.activeFooterText,styles.fontSize10] : [styles.textFooterColor,styles.fontSize10]}>Chats</Text>
						</Button>
						<Button active={(this.state.activeTabs == 5 && !this.state.isOpened)} style={styles.whiteColor} onPress={this.onChangeTabs.bind(this, 5)}>
							<Icon name="person" style={(this.state.activeTabs == 5 && !this.state.isOpened) ? styles.activeFooterText : styles.textFooterColor} />
							<Text style={(this.state.activeTabs == 5 && !this.state.isOpened) ? [styles.activeFooterText,styles.fontSize10] : [styles.textFooterColor,styles.fontSize10]}>Profile</Text>
						</Button>
					</FooterTab>
				</Footer>
			</View>
		)
	}
}
