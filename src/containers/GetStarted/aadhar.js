import React from 'react'
import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { aahdaar } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class AadharCardScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			aadhaarData: {
				aahdaar: ''
			},
			loader: false
		}
	}
	onchange(key, value){
		let {aadhaarData} = this.state;
		aadhaarData[key] = value;
		this.setState({aadhaarData})
	}
	onSubmit(){
		const { aadhaarData } = this.state;
		this.setState({ loader: true })
		if(aadhaarData.aahdaar){
			this.props.dispatch(aahdaar(aadhaarData.aahdaar)).then(res => {
				this.setState({ loader: false })
				if(res.adhaar_data){
					this.setState({ loader: false })
					Actions.ReceiveOtp()
				}
			})
		}else {
			this.setState({ loader: false })
			this.refs.toast.show('Aahdaar field are missing')
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		console.log(this.props,'this.props')
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Get Started" />
				<Content>
					<ProgressBar totalStep={8} currentStep={1} keyboardType='numeric' />
					<View style={styles.mainbox}>
	            <TextBox label="Enter your Aadhar Card no." onchange={this.onchange.bind(this, 'aahdaar')}/>
	            <View>
	            	<View style={styles.flexDirectionRow}>
	            		<Text style={styles.normalText}>Not an Indian Citizen? </Text>
		            	<TouchableOpacity style={styles.forgotLink} onPress={() => {Actions.ForgotPassword()}}>
			          		<Text style={styles.forgotText}>Click here</Text>
			          	</TouchableOpacity>
		          	</View>
		          	<GradientButton label={this.state.loader ? "LOADER": "NEXT"} onPress={this.onSubmit.bind(this)} disabled={this.state.loader ? true: false} pointerEvents={this.state.loader ? 'none': ''}/>
	            </View>
	        </View>
			    <Toast
						ref="toast"
						style={{ backgroundColor: 'rgba(255,0,0,0.4)', width: width - 40 }}
						position='bottom'
						fadeInDuration={1000}
						fadeOutDuration={1000}
						opacity={0.8}
						textStyle={{ color: '#fff',textAlign: 'center' }}
					/>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(AadharCardScreen);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
