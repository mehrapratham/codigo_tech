import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import AutoSuggest from 'react-native-autosuggest';
import { Actions } from 'react-native-router-flux'

export default class SelectCourse extends React.Component{
	render(){
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Select Course" />
				<Content contentContainerStyle={{flex: 1}} style={styles.hasGrayBackground}>
					<ProgressBar totalStep={8} currentStep={4} keyboardType='numeric' />
					<View style={[styles.mainbox, styles.hasFlex]}>
						<View style={styles.autoCompleteCon}>
							<View>
								<Text style={[styles.bigHeading]}>Hi Nikhil!</Text>
							</View>
							<View>
								<Text style={[styles.boxlabel, styles.marginBottom20]}>Profile Type > College Student</Text>
							</View>
							<View>
								<Text style={[styles.boxlabel, styles.marginBottom20, styles.darkBlueColor]}>Enter your course</Text>
								<View style={styles.marginBottom20}>
									<AutoSuggest
							      onChangeText={(text) => console.log('input changing!')}
							      placeholder="Search Course"
							      containerStyles={styles.autoCompleteTextbox}
							      rowWrapperStyles={styles.dropDownStyle}
							      textInputStyles={styles.autoCompleteInput}
							      terms={['Apple', 'Banana', 'Orange', 'Strawberry', 'Lemon', 'Cantaloupe', 'Peach', 'Mandarin', 'Date', 'Kiwi']}
							    />
						    </View>
							</View>
						</View>
	          <View style={styles.alignItBottom}>
	          	<GradientButton label="NEXT" onPress={() => {Actions.resetPassword()}}/>
	          </View>
          </View>
				</Content>
			</Container>
		)
	}
}
