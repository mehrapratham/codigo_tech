import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import { Actions } from 'react-native-router-flux'
export default class SelectProfile extends React.Component{
	render(){
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Select Profile" />
				<Content style={styles.hasGrayBackground}>
					<ProgressBar totalStep={8} currentStep={4} keyboardType='numeric' />
					<View style={styles.mainbox}>
						<View>
							<Text style={[styles.bigHeading]}>Hi Nikhil!</Text>
						</View>
						<View>
							<Text style={[styles.boxlabel, styles.marginBottom10]}>Select your Profile type</Text>
						</View>
						<View style={styles.marginBottom40}>
							<RadioGroup highlightColor="#fff" size={16} color="#9b9b9b" >
				        <RadioButton style={styles.radioBox} value={'item1'} >
				          <Text style={[styles.radioBtnText,styles.darkBlueColor]}>College Student</Text>
				        </RadioButton>
				        <RadioButton style={styles.radioBox} value={'item2'}>
				          <Text style={[styles.radioBtnText,styles.darkBlueColor]}>College Faculty</Text>
				        </RadioButton>
				        <RadioButton style={styles.radioBox} value={'item2'}>
				          <Text style={[styles.radioBtnText,styles.darkBlueColor]}>Company Employee</Text>
				        </RadioButton>
				      </RadioGroup>
						</View>
	          <View>
	          	<GradientButton label="NEXT" onPress={() => {Actions.SelectCourse()}}/>
	          </View>
          </View>
				</Content>
			</Container>
		)
	}
}
