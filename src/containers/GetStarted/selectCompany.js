import React from 'react'
import { View, Text, Image, ScrollView, Dimensions } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import Checkbox from '../../components/Form/Checkbox'
import { Actions } from 'react-native-router-flux'

export default class SelectCompany extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			isChecked: false
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Select Company" />
				<View style={[styles.hasGrayBackground,styles.hasFlex]}>
					<ProgressBar totalStep={8} currentStep={8} keyboardType='numeric' />
					<View style={[styles.hasGrayBackground,styles.hasFlex, {paddingBottom: 40}]}>
						<View style={[styles.mainbox, styles.hasFlex]}>
							<View style={styles.hasFlex}>
								<View>
									<Text style={[styles.bigHeading]}>Hi Nikhil!</Text>
								</View>
								<View>
									<TextBox label="Select the Companies you wish to follow" placeholder="Search for Companies" hasWhitebg={true} leftPadding={20} icon='search' />
								</View>
								<ScrollView style={[styles.checkboxCon, styles.marginBottom20]}>
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
									<Checkbox label="Microsoft Labs" subTitle="Manipal, Karnataka, India" />
								</ScrollView>
							</View>
	          			</View>
	          			<View style={styles.lastGradientView}>
				          	<GradientButton label="DONE" disabled={false} onPress={() => {Actions.Feed()}}/>
			            </View>
			        </View>
				</View>
			</Container>
		)
	}
}
