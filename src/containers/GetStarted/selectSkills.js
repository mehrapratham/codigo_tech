import React from 'react'
import { View, Text, Image, ScrollView, Dimensions } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import Checkbox from '../../components/Form/Checkbox'
import { Actions } from 'react-native-router-flux'

export default class SelectSkills extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			isChecked: false
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Select Skills" />
				<View style={[styles.hasGrayBackground,styles.hasFlex]}>
					<ProgressBar totalStep={8} currentStep={7} keyboardType='numeric' />
					<View style={[styles.hasFlex, {paddingBottom: 40}]}>
						<View style={[styles.mainbox, styles.hasFlex]} alwaysBounceVertical={false}>
							<View style={[styles.hasFlex]}>
								<View>
									<Text style={[styles.bigHeading]}>Hi Nikhil!</Text>
								</View>
								<View>
									<TextBox label="Select your top 5 Interest Skills" leftText="0/5" placeholder="Search for Skills" hasWhitebg={true} leftPadding={20} icon='search' />
								</View>
								<ScrollView style={[styles.checkboxCon, styles.marginBottom20]}>
									<Checkbox label="Management" />
									<Checkbox label="Adobe Photoshop" />
									<Checkbox label="Management" />
									<Checkbox label="Adobe Photoshop" />
									<Checkbox label="Management" />
									<Checkbox label="Adobe Photoshop" />
									<Checkbox label="Management" />
									<Checkbox label="Adobe Photoshop" />
									<Checkbox label="Management" />
								</ScrollView>
							</View>
      			</View>
      			<View style={styles.lastGradientView}>
	          	<GradientButton label="NEXT" onPress={() => {Actions.SelectCollege()}}/>
            </View>
    			</View>
				</View>
			</Container>
		)
	}
}
