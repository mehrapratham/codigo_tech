import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import MemberImages from '../../components/common/MemberImages';
import styles from '../../styles/styles';
import GradientButton from '../../components/Form/GradientButton'

export default class EventSkills extends React.Component {
    render() {
      return (
	      <View style={styles.eventSkillMain}>
	        <View style={styles.eventSkillCon}>
	      		<View style={[styles.errorCon,styles.borderPaddingEvent]}>
	      			<View style={styles.width50}>
	          		<Text style={styles.eventSkillsTextMain}>Public Speaking</Text>
	          	</View>
	          	<View style={[styles.width50,styles.alignItemEnd]}>
	          		<Text><Text style={styles.textColor25}>25</Text><Text style={styles.eventMembersText}> Members</Text> </Text>
	          	</View>
	          </View>
	          <View style={[styles.skillImageOut,styles.borderBottomWidth1,styles.paddingBottomTop15]}>
	              <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
	          </View>
	          <View style={styles.eventSkillsView}>
	            <View style={styles.eventSkillsTextOut}>
	            	<View style={styles.eventSkillsTextIn}>
	            		<Image source={require('../../img/color_dot.png')} style={styles.replyImg}/>
	            	</View>
	            	<View>
	            		<Text><Text style={styles.textColor15}>15</Text><Text style={styles.spotsEventText}> spots available in Public Speaking</Text></Text>
	            	</View>
	            </View>
	          </View>
	      	</View>
	      	<View style={styles.eventSkillCon}>
	      		<View style={[styles.errorCon,styles.borderPaddingEvent]}>
	      			<View style={styles.width50}>
	          		<Text style={styles.eventSkillsTextMain}>Public Speaking</Text>
	          	</View>
	          	<View style={[styles.width50,styles.alignItemEnd]}>
	          		<Text><Text style={styles.textColor25}>25</Text><Text style={styles.eventMembersText}> Members</Text> </Text>
	          	</View>
	          </View>
	          <View style={[styles.skillImageOut,styles.borderBottomWidth1,styles.paddingBottomTop15]}>
	              <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
	          </View>
	          <View style={styles.eventSkillsView}>
	            <View style={styles.eventSkillsTextOut}>
	            	<View style={styles.eventSkillsTextIn}>
	            		<Image source={require('../../img/color_dot.png')} style={styles.replyImg}/>
	            	</View>
	            	<View>
	            		<Text><Text style={styles.textColor15}>15</Text><Text style={styles.spotsEventText}> spots available in Public Speaking</Text></Text>
	            	</View>
	            </View>
	          </View>
	      	</View>
	      	<View style={styles.eventSkillCon}>
	      		<View style={[styles.errorCon,styles.borderPaddingEvent]}>
	      			<View style={styles.width50}>
	          		<Text style={styles.eventSkillsTextMain}>Public Speaking</Text>
	          	</View>
	          	<View style={[styles.width50,styles.alignItemEnd]}>
	          		<Text><Text style={styles.textColor25}>25</Text><Text style={styles.eventMembersText}> Members</Text> </Text>
	          	</View>
	          </View>
	          <View style={[styles.skillImageOut,styles.borderBottomWidth1,styles.paddingBottomTop15]}>
	              <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
	          </View>
	          <View style={styles.eventSkillsView}>
	            <View style={styles.eventSkillsTextOut}>
	            	<View style={styles.eventSkillsTextIn}>
	            		<Image source={require('../../img/color_dot.png')} style={styles.replyImg}/>
	            	</View>
	            	<View>
	            		<Text><Text style={styles.textColor15}>15</Text><Text style={styles.spotsEventText}> spots available in Public Speaking</Text></Text>
	            	</View>
	            </View>
	          </View>
	      	</View>
	      </View>
    	)
  	}
	}