import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon } from 'native-base'
import EventFormHeader from '../../components/Event/EventFormHeader'
import GradientButton from '../../components/Form/GradientButton'
import SelectBox from '../../components/Form/SelectBox'
import TextBox from '../../components/Form/TextBox'
import UploadButton from '../../components/Form/UploadButton';
import { Actions } from 'react-native-router-flux'

export default class EventForm extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			
		}
	}
	render(){
		return(
			<View style={styles.eventFormCon}>
				<EventFormHeader step={1} />
				<View style={styles.eventFormWorkArea}>
					<ScrollView>
						<View style={[styles.formWorkArea, styles.marginTop15, styles.borderBottom]}>
							<View>
								<Text style={styles.formLabel}>Create ab Event</Text>
								<Text style={[styles.formInfoText, styles.marginBottom40]}>Let's create memorable experience through creative event planning and management</Text>
								<View style={styles.twoColumnView}>
									<View style={[styles.firstView]}>
										<SelectBox label="Event Type" labelColor="#6685a3" hasWhitebg={true} width={'95%'} marginBottom={20} height={40} />
									</View>
									<View style={styles.firstView}>
										
									</View>
								</View>

								<TextBox label="Event Title" required={true} labelColor="#6685a3" hasWhitebg={true} placeholder="e.g. Inter Collegiate Cultural Fast" height={40} leftText={"60 Words"} />
								
								<View style={styles.twoColumnView}>
									<View style={[styles.firstView]}>
										<TextBox label="End Data/Time" labelColor="#6685a3" hasWhitebg={true} placeholder="12/10/2018" height={40} width={'95%'} />
									</View>
									<View style={[styles.firstView]}>
										<TextBox label="" hasWhitebg={true} width={60} placeholder="10:10" height={40} />
									</View>
								</View>
								<TextBox label="Event Location" required={true} labelColor="#6685a3" hasWhitebg={true} placeholder="e.g. Inter Collegiate Cultural Fast" height={40} leftText={"60 Words"} />
								<TextBox label="Brief Description" required={true} labelColor="#6685a3" hasWhitebg={true} placeholder="e.g. Inter Collegiate Cultural Fast" height={100} multiline={true} leftText={"250 Words"} marginBottom={1} />
							</View>
						</View>
						<View style={[styles.formWorkArea, styles.marginBottom20]}>
							<View>
								<Text style={styles.formLabel}>Add Tags</Text>
								<Text style={[styles.formInfoText]}>Add upto 5 tags to help viewers know what this event is about</Text>
								<TextBox labelColor="#6685a3" hasWhitebg={true} placeholder="Add Tag" height={40} marginBottom={10} />
								<View>
									<View style={[styles.eventTagsCon]}>
            				<View style={[styles.eventTag, styles.eventTagCon]}>
            					<Text style={styles.eventTagText}>Writing</Text>
            					<TouchableOpacity style={styles.tagClose}>
            						<Icon name="close" style={styles.closeBtnTag} />
            					</TouchableOpacity>
            				</View>
            				<View style={[styles.eventTag, styles.eventTagCon]}>
            					<Text style={styles.eventTagText}>Research</Text>
            					<TouchableOpacity style={styles.tagClose}>
            						<Icon name="close" style={styles.closeBtnTag} />
            					</TouchableOpacity>
            				</View>
            			</View>
								</View>
							</View>
						</View>

						<View>
							<View style={[styles.headingWithBorder, styles.noBorder]}>
								<Text style={styles.headingWithBorderText}>Ticketing Options</Text>
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<View style={styles.twoColumnView}>
									<View style={[styles.firstView]}>
										<SelectBox label="Choose ticketing type" labelColor="#6685a3" hasWhitebg={true} width={'95%'} marginBottom={20} height={40} />
									</View>
									<View style={styles.firstView}>
										
									</View>
								</View>

								<View style={styles.twoColumnView}>
									<View style={[styles.firstView]}>
										<SelectBox label="Choose Currency" labelColor="#6685a3" hasWhitebg={true} width={'95%'} marginBottom={20} height={40} />
									</View>
									<View style={[styles.firstView, {alignItems: 'flex-end'}]}>
										<TextBox label="Amount per person" labelColor="#6685a3" hasWhitebg={true} width={'95%'} placeholder="Amount per person" height={40} />
									</View>
								</View>

								
							</View>
						</View>


						<View>
							<View style={styles.headingWithBorder}>
								<Text style={styles.headingWithBorderText}>Add Skills</Text>
								<Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<TextBox label="Add a skill associated with the event" labelColor="#6685a3" hasWhitebg={true} placeholder="Add Skill" height={40} marginBottom={20} />
								<SelectBox label="Choose Currency" labelColor="#6685a3" hasWhitebg={true} width={150} marginBottom={20} height={40} />
								<TextBox label="Add a participant to the skill" labelColor="#6685a3" hasWhitebg={true} placeholder="Add Member" height={40} marginBottom={20} />
								<View style={[styles.alignRight, styles.paddinBottom20]}>
									<TouchableOpacity>
										<Text style={styles.addLink}>+ Add Skills</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>

						<View>
							<View style={styles.headingWithBorder}>
								<Text style={styles.headingWithBorderText}>Add Spearkers and Guests</Text>
								<Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<View style={styles.marginBottom20}>
									<UploadButton btntext="Upload Picture" />
								</View>
								<TextBox label="Add Guest Name" labelColor="#6685a3" hasWhitebg={true} placeholder="Add Skill" height={40} marginBottom={20} />
								<TextBox label="Guest's Role in the Event" labelColor="#6685a3" hasWhitebg={true} placeholder="Add Role of Guest" height={40} marginBottom={20} />
								<TextBox label="Brief Description about the Guest" labelColor="#6685a3" hasWhitebg={true} placeholder="Enter Description" height={100} multiline={true} marginBottom={20} leftText="250 Words" />
								<View style={[styles.alignRight, styles.paddinBottom20]}>
									<TouchableOpacity>
										<Text style={styles.addLink}>+ Add Guest</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>

						<View style={styles.formButton}>
							<GradientButton label="NEXT" customHeight={35} onPress={() => Actions.AddMedia()} />
						</View>
					</ScrollView>
				</View>
      </View>
		)
	}
}
