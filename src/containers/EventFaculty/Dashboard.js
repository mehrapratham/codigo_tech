import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import styles from '../../styles/styles';
import { Icon } from 'native-base';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import GradientButton from '../../components/Form/GradientButton';
import { LinearGradient } from 'expo';
import { Actions } from 'react-native-router-flux'
import { BarChart, Grid } from 'react-native-svg-charts'

export default class Dashboard extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      barData: [ 50, 20, 35, 53, 73 ],
      fill : 'rgb(134, 65, 244)'
    }
  }
  
  render() {
      let activeProgress = (100 / 4)
      activeProgress = activeProgress * this.props.step + '%'
      return (
          <View style={styles.postCon}>
              <View style={[styles.postHeader, { backgroundColor: '#fbfbfb' }]}>
                  <TouchableOpacity onPress={() => { Actions.pop() }}>
                      <Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
                  </TouchableOpacity>
                  <View style={styles.nextToIcon}>
                      <Text>Events</Text>
                  </View>
              </View>
              <ScrollView>
                <View style={[styles.dashMain, styles.marginBottom20]}>
                  <Text style={styles.dashText}>Dashboard</Text>
                  <View style={styles.dashCon}>
                    <View style={styles.dashTitleMain}>
                      <Text style={styles.dashTextMain}>Workshop on Web Applictions</Text>
                      <View style={styles.dashTitleDots} >
                        <Image source={require('../../img/dots.png')} />
                      </View>
                    </View>
                    <Text style={styles.dashTextTime}>Event Created on: 20th may 2018 at 2:20pm</Text>
                      <View style={styles.dashConMain}>
                        <View style={styles.dashTextAligne}>
                          <Text style={styles.dashTextTitle}>Request to join the Event</Text>
                          <Text style={styles.dashTextAll}>View All</Text>
                        </View>
                        <View style={styles.dashConBar}>
                          <View style={styles.marginTop5}>
                            <RadioGroup color='grey' activeColor="orange" size={20} thickness={1}>
                              <RadioButton value={'item1'} >
                                <Text style={styles.dashInnerTest}>Request Target</Text>
                              </RadioButton>
                              <RadioButton value={'item2'} >
                                <Text style={styles.dashInnerTest}>Request received</Text>
                              </RadioButton>
                            </RadioGroup>
                          </View>
                          <View style={styles.barChartCon}>
                            <BarChart
                              style={{ height: 200 }}
                              data={ this.state.barData }
                              svg ={{fill: '#f28c6b'}}
                              spacingInner={0.3}
                              spacingOuter={0.2}
                              animate={true}
                              barSpacing={5}
                              contentInset={{ top: 30, bottom: 30 }}>
                            </BarChart>
                          </View>
                          <View style={styles.dashBarTitleOut}>
                            <Text style={styles.dashBarTitle}>HTML and CSS</Text>
                            <Text style={styles.dashBarDetails}>(No Request Received)</Text>
                          </View>
                        </View>
                        <View style={styles.dashBox}>
                          <View style={[styles.dashInnerBox, styles.noRightBorder]}>
                            <Text style={styles.dashBoxText}>Number of slots</Text>
                            <Text style={styles.dashBoxInnerNumber}>54</Text>
                          </View>
                          <View style={styles.dashInnerBox}>
                            <Text style={styles.dashBoxText}>Number of Request</Text>
                            <Text style={styles.dashBoxInnerNumber}>--</Text>
                          </View>
                        </View>
                        <View style={styles.dashBtnOut}>
                          <GradientButton label="VIEW DETAILS" customHeight={35} smallGradientText={styles.dashBtnText} />
                        </View>
                      </View>
                  </View>
                  <View style={{ backgroundColor: '#fdfdfe', }}>
                    <View style={styles.dashInviteBox}>
                      <Text style={styles.dashInviteText}>Invited</Text>
                      <View style={styles.dashBoxtextOut}>
                        <Text style={styles.dashInviteTextmember}>0 members</Text>
                        <Icon name="arrow-forward" style={styles.dashBoxIcon} />
                      </View>
                      <View style={[styles.dashProgressBarActive]}>
                        <View style={{width: 35+'%'}}>
                          <LinearGradient
                            colors={['#69c3f6', '#9c89f2']}
                            style={[styles.dashGradientBtn]}
                            start={[0.1, 0.1]} end={[1.0, 0.3]}>
                          </LinearGradient>
                        </View>
                      </View>
                      <Text style={styles.dashInviteText}>Change 35%</Text>
                    </View>
                    <View style={[styles.dashInviteBox, styles.marginBottom20]} >
                      <Text style={styles.dashInviteText}>Invited</Text>
                      <View style={styles.dashBoxtextOut}>
                        <Text style={styles.dashInviteTextmember}>0 members</Text>
                        <Icon name="arrow-forward" style={styles.dashBoxIcon} />
                      </View>
                      <View style={[styles.dashProgressBarActive]}>
                        <View style={{width: 80+'%'}}>
                          <LinearGradient
                            colors={['#69c3f6', '#9c89f2']}
                            style={[styles.dashGradientBtn]}
                            start={[0.1, 0.1]} end={[1.0, 0.3]}>
                          </LinearGradient>
                        </View>
                      </View>
                      <Text style={styles.dashInviteText}>Change 80%</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.DashFormButton}>
                  <GradientButton label="VIEW EVENT" customHeight={37} smallGradientText={{ letterSpacing: 0 }} />
                </View>
              </ScrollView>
          </View>
      )
  }
}