import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import OtpInputs from 'react-native-otp-inputs'
import { Actions } from 'react-native-router-flux'

export default class SecurityCode extends React.Component{
	_onFulfill(code){
		console.log(code)
	}
	render(){
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Security Code" />
				<Content>
					<View style={[styles.mainbox,styles.paddingLeft10]}>
						<View>
							<Text style={[styles.boxlabel,styles.marginBottomZero]}>Enter the Security code to your registered</Text>
							<Text style={[styles.fontSize12,styles.darkBlueColor]}>Email ID</Text>
						</View>
					</View>
					<View style={[styles.marginBottom30]}>
						<View style={{alignItem: 'center'}}>
				    </View>
						<OtpInputs handleChange={code => console.log(code)} inputStyles={styles.otpColor} numberOfInputs={4} inputContainerStyles={[styles.otpInputColor]} focusedBorderColor="#55bf8f" keyboardType="numeric-pad" />
						<View style={styles.centered}><TouchableOpacity><Text style={styles.resendCodeText}>Resend Code</Text></TouchableOpacity></View>
					</View>
					<View style={styles.mainbox}>	
		            	<View>
			          		<GradientButton label="NEXT" onPress={() => {Actions.ResetPassword()}}/>
				        </View>
    				</View>
				</Content>
			</Container>
		)
	}
}
