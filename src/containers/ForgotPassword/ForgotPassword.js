import React from 'react'
import { View, Text, TouchableOpacity, TextInput } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import { Actions } from 'react-native-router-flux'

export default class ForgotPassword extends React.Component{
	render(){
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Forgot Password" />
				<Content>
					<View style={styles.mainbox}>
						<View>
							<Text style={[styles.boxlabel, styles.marginBottom10]}>Where would you like to get your security code?</Text>
						</View>
						<View style={styles.marginBottom40}>
							<View style={styles.marginBottom15}>
								<RadioGroup highlightColor="#f0f3f6" size={16} color="#9d9d9d" >
					        <RadioButton style={[styles.radioBox, styles.marginBottomZero, {backgroundColor: '#f0f3f6'}]} value={'item1'} >
					          <Text style={styles.radioBtnText}>Email</Text>
					        </RadioButton>
				        </RadioGroup>
				        <View style={styles.selectOpenView}>
				       		<TextInput placeholder="shawaz@codigoworld.com" style={styles.textInputWithoutLabel}/> 
				        </View>
			        </View>
			        <RadioGroup highlightColor="#f0f3f6" size={16} color="#9d9d9d" >
				        <RadioButton style={[styles.radioBox, styles.marginBottomZero, {backgroundColor: '#f0f3f6'}]} value={'item2'} >
				          <Text style={styles.radioBtnText}>Phone</Text>
				        </RadioButton>
			        </RadioGroup>
			        <View style={styles.selectOpenView}>
			       		<TextInput placeholder="+91 9877678203" style={styles.textInputWithoutLabel}/> 
			        </View>
						</View>
          	<View>
          		<GradientButton label="SEND OTP" onPress={() => {Actions.SecurityCode()}}/>
          	</View>
    			</View>
				</Content>
			</Container>
		)
	}
}
