import React from 'react'
import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import BlockButton from '../../components/Form/BlockButton'

import styles from '../../styles/styles'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { login } from '../../actions/Auth'
import { IsValidForm } from '../../components/common/validation'
import Toast from 'react-native-easy-toast'

class Login extends React.Component{
	constructor(props){
		super(props)
		this.state ={
			loginData: {
				username: '',
				password: ''
			},
			loader: false
		}
	}
	onchange(key, value){
		let {loginData} = this.state;
		loginData[key] = value;
		this.setState({loginData})
	}

	onSubmit() {
		const { loginData } = this.state
		let fields = ['username', 'password']
		let formValidation = IsValidForm(fields, loginData)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			this.setState({ loader: true })
			this.props.dispatch(login(loginData)).then(res => {
				this.setState({ loader: false })
				console.log(res.token,66)
				if (res.token) {
					this.setState({ loader: false })
					Actions.Feed()
				}
				if (res.non_field_errors) {
					this.setState({ loader: false })
					return (this.refs.toast.show(res.non_field_errors))
				}
			})
		} else {
			this.refs.toast.show('Some field are missing')
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		console.log(this.state.loginData, 123)
		console.log(this.props,'thisprops')
		return(
			<Container style={styles.topBox}>
				<Content>
					<View style={styles.mainHeading}>
						<Text style={styles.mainHeadingtext}>SIGN IN</Text>
					</View>
					<View style={styles.mainbox}>
	          <TextBox label="Phone Number" onchange={this.onchange.bind(this, 'username')} />
	          <TextBox label="Password" secureTextEntry={true} onchange={this.onchange.bind(this, 'password')} rightLink="Forgot Password?" labelLinkColor="#5ba7e8"/>
	          <View>
	          	<BlockButton label={this.state.loader ? "LOADER": "SIGN IN"} disabled={this.state.loader} onPress={this.onSubmit.bind(this)}/>
	          </View>
	          <View style={styles.btmLink}>
	          	<Text style={styles.btmLinkText}>
	          		<Text style={styles.btnLink}>New User? </Text>
		            <Text style={[styles.btnLinkText]} onPress={() => {Actions.AadharCardScreen()}}>Sign Up</Text>
	          	</Text>
	          </View>
          </View>
          <Toast
						ref="toast"
						style={{ backgroundColor: 'rgba(255,0,0,0.4)', width: width - 40 }}
						position='bottom'
						fadeInDuration={1000}
						fadeOutDuration={1000}
						opacity={0.8}
						textStyle={{ color: '#fff',textAlign: 'center' }}
					/>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(Login);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
