import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo'
import GetStartedButton from '../../components/Form/GetStartedButton'
import styles from '../../styles/styles1'
import { Actions } from 'react-native-router-flux'
export default class BuildScreen extends React.Component{
	render(){
		return(
			<LinearGradient
		        colors={['#3fa2f7', '#6580ea']}
			    style={styles.container}
		        start={[1, 0.9]} end={[1, 0.1]}
			>
				<View style={styles.firstCon}>
					<Text style={styles.textStyle}>Get recognized by companies</Text>
				</View>
				<View style={styles.secondCon}>
					<View style={styles.imgCon}>
						<Image source={require('../../img/slider3.png')} style={styles.img}/>
					</View>
				</View>
				<View style={styles.thirdCon}>
					<View style={styles.lastView}>
						<View style={[styles.dotStyle,{backgroundColor: 'rgba(255,255,255,0.7)'}]}></View>
						<View style={[styles.dotStyle,{backgroundColor: 'rgba(255,255,255,0.7)'}]}></View>
						<View style={[styles.dotStyle,{backgroundColor: '#fff'}]}></View>
					</View>
					<View style={styles.seconLastView}>
						<TouchableOpacity onPress={() => {Actions.Login()}}><Text style={styles.loginText}>Already a member ? Log In</Text></TouchableOpacity>
					</View>
					<View style={styles.thirdLastView}>
						<GetStartedButton onPress={() => {Actions.AadharCardScreen()}}/>
					</View>
				</View>
			</LinearGradient>
		)
	}
}