import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'
import GetStartedButton from '../../components/Form/GetStartedButton'
import { LinearGradient } from 'expo'
import styles from '../../styles/styles1'
export default class BuildScreen extends React.Component{
	componentWillMount(){
		setTimeout(function(){
			Actions.Recognized()
    	}, 2000);
	}
	render(){
		return(
			<LinearGradient
		        colors={['#3fa2f7', '#6580ea']}
			    style={styles.container}
		        start={[1, 0.9]} end={[1, 0.1]}
			>
				<View style={styles.firstCon}>																																																																
					<Text style={styles.textStyle}>Build Your Skills</Text>
				</View>
				<View style={styles.secondCon}>
					<View style={styles.imgCon}>
						<Image source={require('../../img/slider2.png')} style={styles.img}/>
					</View>
				</View>
				<View style={styles.thirdCon}>
					<View style={styles.lastView}>
						<View style={[styles.dotStyle,{backgroundColor: 'rgba(255,255,255,0.7)'}]}></View>
						<View style={[styles.dotStyle,{backgroundColor: '#fff'}]}></View>
						<View style={[styles.dotStyle,{backgroundColor: 'rgba(255,255,255,0.7)'}]}></View>
					</View>
					<View style={styles.seconLastView}>
						<Text style={styles.loginText}>Already a member ? Log In</Text>
					</View>
					<View style={styles.thirdLastView}>
						<GetStartedButton />
					</View>
				</View>
			</LinearGradient>
		)
	}
}