import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import MemberImages from '../../components/common/MemberImages';
import styles from '../../styles/styles';
import GradientButton from '../../components/Form/GradientButton'

export default class Skills extends React.Component {
    render() {
        return (
            <View style={styles.skillMain}>
                <View style={styles.skillCon}>
                    <Text style={styles.skillsTextMain}>HTML and CSS</Text>
                    <View style={styles.skillImageOut}>
                        <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
                    </View>
                    <View style={styles.SkillsTextOut}>
                        <Text style={styles.skillText}>15 spot available in HTML and CSS</Text>
                        <View style={styles.skillBtnOut}>
                            <GradientButton label="Join a Skill" smallGradientButtn={styles.skillTouchBtn} smallGradientText={styles.skillTouchText} isFollow={true} />
                        </View>
                    </View>
                    <View style={styles.lineStyle} />
                </View>
                <View style={styles.skillCon}>
                    <Text style={styles.skillsTextMain}>JavaScript</Text>
                    <View style={styles.skillImageOut}>
                        <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
                    </View>
                    <View style={styles.SkillsTextOut}>
                        <Text style={styles.skillText}>15 spot available in JavaScript</Text>
                        <View style={styles.skillBtnOut}>
                            <GradientButton label="Join a Skill" smallGradientButtn={styles.skillTouchBtn} smallGradientText={styles.skillTouchText} isFollow={true} />
                        </View>
                    </View>
                    <View style={styles.lineStyle} />
                </View>
                <View style={styles.skillCon}>
                    <Text style={styles.skillsTextMain}>User Interface</Text>
                    <View style={styles.skillImageOut}>
                        <MemberImages memberInner={styles.skillsImgOut} memberImg={styles.skillImg} />
                    </View>
                    <View style={styles.SkillsTextOut}>
                        <Text style={styles.skillText}>15 spot available in User Interface</Text>
                        <View style={styles.skillBtnOut}>
                            <GradientButton label="Join a Skill" smallGradientButtn={styles.skillTouchBtn} smallGradientText={styles.skillTouchText} isFollow={true} />
                        </View>
                    </View>
                    <View style={styles.lineStyle} />
                </View>
            </View>
        )
    }

}