import React from 'react'
import { Container, Content } from 'native-base'
import { View, Text, Image } from 'react-native'
import styles from '../../styles/styles'

export default class CongratulationScreen extends React.Component{
	render(){
		return(
			<Container>
				<Content>
					<View style={styles.mainbox}>
						<View style={[styles.mainbox,styles.marginBottomWidth1]}>
							<View style={[styles.btmLinkText,styles.marginBottom10]}>
								<Image source={require('../../img/congratulation.png')}/>
							</View>
							<Text style={styles.congratulationText}>Congratulations</Text>
							<Text style={[styles.blueColorOrfontSize11,styles.marginBottom10,styles.letterSpacing1]}>you have successfully joined the skill "HTML and CSS in WorkShop on Web Development" </Text>
						</View>
						<View style={styles.mainbox}>
							<View style={[styles.rowDirection,styles.marginBottom10]}>
								<View style={styles.width50}>
									<Text style={styles.greyColorFontSize12}>Booking ID -</Text>
								</View>
								<View style={styles.width50}>
									<Text style={styles.darkGreyfontSize12}>NF02046754326</Text>
								</View>
							</View>
							<View style={[styles.rowDirection,styles.marginBottom10]}>
								<View style={styles.width50}>
									<Text style={styles.greyColorFontSize12}>Booking ID -</Text>
								</View>
								<View style={styles.width50}>
									<Text style={styles.darkGreyfontSize12}>NF02046754326</Text>
								</View>
							</View>
							<View style={styles.personRelativeView}>
								<View style={styles.personAbsoluteView}>
									<Image source={require('../../img/user.png')} style={styles.replyImg}/>
								</View>
								<Text style={styles.nikhilText}>Nikhil Kashyap</Text>
							</View>
							<View style={styles.marginTop15}>
								<Text style={[styles.darkGreyfontSize16,styles.marginBottom10]}>Workshop on Web Development</Text>
								<Text style={[styles.timeSize,styles.colorDarkgrey]}>17th May | 10am to 4pm</Text>
								<Text style={styles.timeSize}>Dr.TMA pai Hall, Manipal University</Text>
								<Text style={[styles.marginTop15,styles.marginBottom15]}><Text style={styles.timeSize}>Applied for Skill:</Text> <Text style={styles.darkGreyfontSize12}>HTML and CSS</Text></Text>
								<Text style={styles.fontSize11Dark}>You can collect your tickets from the kios near the auditorium</Text>
							</View>
						</View>
					</View>
				</Content>
			</Container>
		)
	}
}