import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import {Video} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import FeedActions from '../../components/common/FeedActions'
import AddComment from '../../components/common/AddComment'
import WrittenBy from '../../components/common/WrittenBy'
import MemberImages from '../../components/common/MemberImages'
import { Actions } from 'react-native-router-flux'


export default class ArticleDetail extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			currentTags: ['Writing', 'Marketing'],
			participants: ['Rupinderpal', 'Pratham']
		}
	}
	
	render(){
		return(
			<View style={styles.postCon}>
				<View style={styles.postHeader}>
					<TouchableOpacity onPress={() => {Actions.pop()}}>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text>Articles</Text>
					</View>
					<View style={styles.rightSide}>
						
					</View>
				</View>
        <View style={styles.workingArea}>
        	<ScrollView>
	        	<View style={[styles.formCon, styles.bringToTop]}>
	        		<Text style={[styles.formHeading, styles.subHeading, styles.marginBottom10]}>Create a Market Mix</Text>
	        		<Text style={styles.atricleText}>They all have access to a formula. A set of defining principles.</Text>
	        		<Text style={styles.atricleText}>And if followed, the likelihood of success for any marketing communication goes up significantly. I figured this out through 10 years of research, and more than $150,000 on different projects and campaigns.</Text>
	        		<View style={styles.imgBox}>
		        		<Image source={require('../../img/article.png')} style={styles.articleImg} />
		        		<Image source={require('../../img/article1.png')} style={styles.articleImg} />
		        		<VideoPlayer
	               	videoProps={{
	                 	shouldPlay: false,
	                 	resizeMode: Video.RESIZE_MODE_CONTAIN,
	                 	source: {
	                   	uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
	                 	},
	               	}}
	                isPortrait={true}
	                playFromPositionMillis={0}
	                style={{width: '100%'}}
	              />
		        	</View>
		        	<View style={styles.writenByCon}>
		        		<Text style={[styles.formHeading, styles.subHeading, styles.smallTextHeading, styles.marginBottom10]}>Written By</Text>
		        		<WrittenBy size={65} />
		        	</View>
	        	</View>
	        	<View style={styles.membersCon}>
	        		<View style={styles.marginBottom30}>
	        			<Text style={[styles.formHeading, styles.subHeading, styles.smallTextHeading]}>Research</Text>
	        			<Text style={styles.rightText}>
	        				<Text style={styles.memberCount}>26 </Text>
	        				<Text style={styles.memberLabel}>Members</Text>
	        			</Text>
	        		</View>
	        		<View style={styles.marginBottom40}>
	        			<MemberImages />
	        		</View>

	        		<View style={styles.marginBottom30}>
	        			<Text style={[styles.formHeading, styles.subHeading, styles.smallTextHeading]}>Marketing</Text>
	        			<Text style={styles.rightText}>
	        				<Text style={styles.memberCount}>26 </Text>
	        				<Text style={styles.memberLabel}>Members</Text>
	        			</Text>
	        		</View>

	        		<View>
	        			<MemberImages />
	        		</View>

	        	</View>
	        	<View style={styles.tagsCon}>
	        		<Text style={[styles.formHeading, styles.subHeading, styles.smallTextHeading]}>Tags</Text>
	        		<ChipsBox inputPlaceholder="Search Tag" chips={this.state.currentTags} onlyTags={true} />
	        	</View>
	        	<View style={styles.articleActions}>
	        		<View>
								<FeedActions />
							</View>
					<View>
						<AddComment />
					</View>
        		</View>
        	</ScrollView>
        </View>
      </View>
		)
	}
}
