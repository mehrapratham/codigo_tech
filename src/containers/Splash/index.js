import React from 'react'
import { View, Text, Image } from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import styles from '../../styles/styles1'
class Splash extends React.Component{
	componentWillMount(){
		setTimeout(function(){
			Actions.StartScreen()
    	}, 2000);
	}
	render(){
		return(
			<View style={styles.secondCon}>
				<View style={styles.imgCon}>
					<Image source={require('../../img/splash.png')} style={styles.img}/>
				</View>
			</View>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Splash);

const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}