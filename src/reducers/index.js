import { combineReducers } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import Auth from './Auth';
import GetStarted from './GetStarted'
import Article from './Article'
/*combine reducers*/
export default combineReducers({
 router: routerReducer,
 Auth: Auth,
 GetStarted: GetStarted,
 Article: Article
});