import initialState from '../initialState'
import { GET_AAHDAAR_DATA, GET_OTP_DATA } from '../../actions/types'
export default (state = initialState.getStarted, action) => {
 switch (action.type) {
   case GET_AAHDAAR_DATA:
    return { ...state, aadharData: action.data.adhaar_data }
   case GET_OTP_DATA:
    return { ...state, otpData: action.data }
   default:
    return state
 }
}