import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'

export default class FeedUser extends React.Component{
	render(){
		return(
			<View style={styles.userInfoCon}>
        <Image source={require('../../img/user.png')} style={[styles.userImg, this.props.isSmall && {width: this.props.size, height: this.props.size, marginTop: -(this.props.sliderUpImage)}]} />
        <View style={[styles.userInfo, this.props.isSmall && {left: this.props.size+10}]}>
          <Text style={styles.userName}>
            <Text style={styles.userNameText}>Rupinderpal </Text>
            {this.props.userAction && <Text style={styles.nextToName}>commented on post</Text> }
          </Text>
          <Text style={styles.userSubTitle}>Student-NITK</Text>
          {!this.props.isSmall && <Text style={styles.minAgo}>20 minute ago</Text>}
        </View>
        <View style={styles.rightContainer}>
          {!this.props.isSmall && <TouchableOpacity>
              <Image source={require('../../img/bookmark.png')} />
            </TouchableOpacity>
          }
        </View>
      </View>
		)
	}
}