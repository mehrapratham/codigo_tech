import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import PopupDialog from 'react-native-popup-dialog';
import GradientButton from '../Form/GradientButton'
var { height, width } = Dimensions.get('window');

export default class Popup extends React.Component {
   render() {
       return (
            <PopupDialog
              ref={this.props.link}
              width={width}
              height={180}
              dialogStyle={this.props.style}
              dismissOnTouchOutside={false}
            >
               <View>
                  <Text style={styles.textSize}>
                    {this.props.label}
                  </Text>
                  <Text style={styles.textDescription}>
                    {this.props.description}
                  </Text>
                  <View style={{alignItems: 'center'}}>
                    <View style={styles.btnGradient}>
                        <GradientButton label={this.props.btnText} onPress={this.props.onPress} />
                    </View>
                  </View>
               </View>
           </PopupDialog>
       )
   }
}
const styles = StyleSheet.create({
   textSize: {
       fontSize: 27,
       fontWeight: 'bold',
       color: '#fff',
       textAlign: 'center'
   },
   textDescription: {
       fontSize: 16,
       color: '#fff',
       textAlign: 'center',
       paddingTop: 10,
       paddingBottom: 40
   },
   btnGradient: {
       width: 120,
   }
})