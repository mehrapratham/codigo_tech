import React, { Component } from 'react';
import { Image, View } from 'react-native'
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Button } from 'native-base';
import styles from '../../styles/styles'
import GradientButton from '../Form/GradientButton'
export default class LikesList extends Component {
  render() {
    return (
      <ListItem avatar noIndent style={[styles.paddingTop0,this.props.list.isVar && styles.likePageBackground]} noBorder>
        <Left>
          <Thumbnail source={require('../../img/user.png')} style={styles.thumbnailStyle}/>
        </Left>
        <Body>
          <Text style={[styles.chatUpperText,styles.fontSize13,styles.marginTop10]}>{this.props.list.name}</Text>
          <Text note style={[styles.notificationText,styles.darkGreyColor,styles.checkboxTitle]}>{this.props.list.text}</Text>
        </Body>
        <Right>
          <View style={styles.marginTop15}>
            <GradientButton label={this.props.list.button} smallGradientButtn={styles.smallGradientButtn} smallGradientText={styles.smallGradientText} isFollow={this.props.list.isFollow}/>
          </View>
        </Right>
      </ListItem>
    );
  }
}
