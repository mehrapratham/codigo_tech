import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import styles from '../../styles/styles'
import ReplyAction from './ReplyAction'

export default class CommentSection extends React.Component{
	render(){
		return(
			<View>
				<View style={[styles.commentBox, styles.marginBottom10]}>
					<View style={styles.commentAvatar}>
						<Image source={require('../../img/user_sm.png')} />
					</View>	
					<View style={styles.commentRightBox}>
						<Text style={styles.nameComment}>Rupinderpal Singh</Text>
						<Text style={styles.timeComment}>18 min</Text>
						<Text style={styles.subTitleComment}>Student-NITK</Text>
						<Text style={styles.commentText}>A very interesting course indeed. I am learning a lot about design and prototyping!</Text>
					</View>
					{this.props.showDetail && <ReplyAction like="3" reply="10" />}
					{this.props.showDetail && 
						<View style={styles.replyCon}>
							<TouchableOpacity style={styles.showReplyLink}>
								<Text style={styles.replyLinkText}>Show pervious replies</Text>
							</TouchableOpacity>
							<View style={styles.marginBottom10}>
								<View style={[styles.commentAvatar, styles.replyUserImg]}>
									<Image source={require('../../img/user_sm.png')} style={styles.replyImg} />
								</View>	
								<View style={styles.commentRightBox}>
									<Text style={styles.nameComment}>Rupinderpal Singh</Text>
									<Text style={styles.timeComment}>18 min</Text>
									<Text style={styles.subTitleComment}>Student-NITK</Text>
									<Text style={styles.commentText}>A very interesting course indeed. I am learning a lot about design and prototyping!</Text>
								</View>
								<ReplyAction like="3" />
							</View>
							<View style={styles.marginBottom10}>
								<View style={[styles.commentAvatar, styles.replyUserImg]}>
									<Image source={require('../../img/user_sm.png')} style={styles.replyImg} />
								</View>	
								<View style={styles.commentRightBox}>
									<Text style={styles.nameComment}>Rupinderpal Singh</Text>
									<Text style={styles.timeComment}>18 min</Text>
									<Text style={styles.subTitleComment}>Student-NITK</Text>
									<Text style={styles.commentText}>A very interesting course indeed. I am learning a lot about design and prototyping!</Text>
								</View>
							</View>
						</View>
					}
					
				</View>
			</View>
		)
	}
}