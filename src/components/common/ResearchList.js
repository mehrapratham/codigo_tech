import React, { Component } from 'react';
import { Image, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Button } from 'native-base';
import styles from '../../styles/styles'
import GradientButton from '../Form/GradientButton'
export default class ResearchList extends Component {
  render() {
    return (
      <View style={styles.researchOuterView}>
        <View style={styles.avatarResearch}>
          <Image source={require('../../img/user.png')} style={styles.memberImg}/>
        </View>
        <View style={styles.mainViewResearch}>
          <View>
            <Text style={[styles.researchUpperText]}>Calvin Holland</Text>
            <Text style={[styles.notificationText,styles.darkLightColor,styles.checkboxTitle]}>Faculty, Manipal University</Text>
          </View>
          <View style={styles.crossResearch}>
            <TouchableOpacity activeOpacity={0.5}>
              <Image source={require('../../img/cross.png')} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
