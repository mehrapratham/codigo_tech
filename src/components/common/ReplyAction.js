import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Icon } from 'native-base'
import styles from '../../styles/styles'
import { LinearGradient } from 'expo';

export default class ReplyAction extends React.Component{
	render(){
		return(
			<View style={styles.replyActionCon}>
				<View style={styles.marginRight10}> 
					<TouchableOpacity>
						<Text style={styles.actionTextCon}>
							<Image source={require('../../img/like.png')} style={styles.actionImg}/>
							<Text style={styles.actionText}>Likes</Text>
						</Text>
					</TouchableOpacity>
				</View>
				<View style={styles.marginRight10}>
					<TouchableOpacity>
						<Text style={styles.actionTextCon}> 
							<Image source={require('../../img/comment.png')} style={styles.actionImg}/>
							<Text style={styles.actionText}>Reply</Text>
						</Text>
					</TouchableOpacity>
				</View>
				<View style={styles.replyCountCon}>
					{this.props.like && <TouchableOpacity>
						<Text style={styles.replyCount}>{this.props.like} likes</Text>
					</TouchableOpacity>}
					{this.props.reply &&
						<View style={styles.divider}></View>
					}
					{this.props.reply &&
						<TouchableOpacity>
							<Text style={styles.replyCount}>{this.props.reply} Reply</Text>
						</TouchableOpacity>
					}
				</View>
			</View>
		)
	}
}