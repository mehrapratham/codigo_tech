import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'

export default class WrittenBy extends React.Component{
	render(){
		return(
			<View style={styles.userInfoCon}>
        <Image source={require('../../img/user.png')} style={[styles.userImg, {width: this.props.size, height: this.props.size, marginTop: -(this.props.sliderUpImage)}]} />
        <View style={[styles.userInfo, styles.writtenByInfo, {left: this.props.size+10, paddingRight: this.props.size+5}]}>
          <Text style={styles.userName}>
            <Text>Rupinderpal </Text>
          </Text>
          <Text style={styles.userSubTitle}>Student at Manipal University</Text>
          {!this.props.isSmall && <Text style={styles.minAgo}>Mechanical Engineer with a passion for design and management</Text>}
        </View>
      </View>
		)
	}
}