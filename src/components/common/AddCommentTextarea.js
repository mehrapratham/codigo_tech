import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import styles from '../../styles/styles'

export default class AddComment extends React.Component{
	render(){
		return(
			<View style={[styles.commentBox, styles.commentBoxTextArea]}>
				<View style={styles.commentAvatar}>
					<Image source={require('../../img/user_sm.png')} />
				</View>	
				<TextInput style={[styles.commentInput, styles.commentTextArea]} underlineColorAndroid="transparent" placeholder="You can post a question or share your views here..." multiline={true}/>
				<View style={[styles.commentIconsCon, styles.commentIconsTextareaCon]}>
					<TouchableOpacity style={styles.commentIcon}>
						<Image source={require('../../img/smile.png')} style={[styles.commentIconBtn2]}/>
					</TouchableOpacity>
					<TouchableOpacity style={[styles.commentIcon, {marginTop: -1}]}>
						<Image source={require('../../img/shape_copy.png')} style={[styles.commentIconBtn]}/>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}