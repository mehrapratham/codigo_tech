import React, { Component } from 'react';
import { Image } from 'react-native'
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import styles from '../../styles/styles'
export default class ChatList extends Component {
  render() {
    let unSeenMsgName = this.props.list.msgUnSeen ? <Text style={styles.msgUnseenName}>{this.props.list.name}</Text>:<Text style={styles.chatUpperText}>{this.props.list.name}</Text>
    let unSeenMsgText = this.props.list.msgUnSeen ? <Text style={styles.msgUnseenText}>{this.props.list.text}</Text>:<Text style={styles.chatText}>{this.props.list.text}</Text>
    return (
      <ListItem avatar noIndent style={[styles.paddingTop0,styles.listBorder]} noBorder>
        <Left>
          <Thumbnail source={require('../../img/user.png')} style={styles.thumbnailStyle}/>
        </Left>
        <Body>
          <Text style={[styles.marginTop10]}>{unSeenMsgName}</Text>
          <Text style={styles.marginTop3}>{unSeenMsgText}</Text>
        </Body>
        <Right>
          <Text note style={[styles.chatTimeText,styles.marginTop10]}>{this.props.list.time}</Text>
        </Right>
      </ListItem>
    );
  }
}
