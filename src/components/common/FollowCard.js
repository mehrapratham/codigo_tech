import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from '../../styles/styles'

export default class FollowCard extends React.Component{
	render(){
		return(
			<View style={[styles.followCard, {width: this.props.width}]}>
				<Image source={require('../../img/followUni.png')} />
				<Text style={styles.followTitle}>{this.props.data && this.props.data.title}</Text>
				<Text style={styles.followSubTitle}>{this.props.data && this.props.data.subTitle}</Text>
				{this.props.data && this.props.data.mutualCon && <Text style={styles.mutualConText}>{this.props.data.mutualCon} Mutual Connection</Text>}
				<Text style={styles.borderBox}></Text>
				<TouchableOpacity>
					<Text style={styles.followLink}>Follow</Text>
				</TouchableOpacity>
			</View>
		)
	}
}