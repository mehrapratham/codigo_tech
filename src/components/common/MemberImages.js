import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import styles from '../../styles/styles'

export default class MemberImages extends React.Component {
  render() {
    return (
      <View style={[styles.membersImgCon]}>
        <View style={styles.memberImgBox}>
          <View style={[styles.memberInner, this.props.memberInner]}>
            <Image source={require('../../img/user.png')} style={[styles.memberImg , this.props.memberImg]} />
          </View>
        </View>
        <View style={styles.memberImgBox}>
          <View style={[styles.memberInner, this.props.memberInner]}>
            <Image source={require('../../img/user.png')} style={[styles.memberImg , this.props.memberImg]}  />
          </View>
        </View>
        <View style={styles.memberImgBox}>
          <View style={[styles.memberInner, this.props.memberInner]}>
            <Image source={require('../../img/user.png')} style={[styles.memberImg , this.props.memberImg]}  />
          </View>
        </View>
        <View style={styles.memberImgBox}>
          <View style={[styles.memberInner, this.props.memberInner]}>
            <Image source={require('../../img/user.png')} style={[styles.memberImg , this.props.memberImg]}  />
          </View>
        </View>
        <View style={styles.memberImgBox}>
          <View style={[styles.memberInner, this.props.memberInner]}>
            <Image source={require('../../img/user.png')} style={[styles.memberImg , this.props.memberImg]}  />
            <View style={styles.haveMore}>
              <Text style={styles.whiteText}>+21</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}