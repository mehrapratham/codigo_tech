import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon } from 'native-base'

export default class UploadButton extends React.Component {
    render() {
        return (
            <View style={styles.amTouchOut}>
                <TouchableOpacity style={styles.amTouch}>
                    <Text style={styles.amTouchTextBtn}>{this.props.btntext}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}