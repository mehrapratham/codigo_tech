import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo';
import styles from '../../styles/styles'

export default class GradientButton extends React.Component{
  render(){
    let primaryColor = this.props.disabled ? '#b2b2b2' : '#58c075'
    let secondaryColor = this.props.disabled ? '#b2b2b2' : '#4cb9d7'
    return(
      <TouchableOpacity onPress={this.props.onPress} style={this.props.customWidth && {width: this.props.customWidth}}>
        <LinearGradient
          colors={[primaryColor, secondaryColor]}
          style={[styles.gradientBtn, this.props.smallGradientButtn, {padding: 1}, this.props.customHeight && {height: this.props.customHeight}]}
          start={[0.1, 1.0]} end={[1.3, 0.3]}
        >
          <View style={this.props.isFollow && styles.followButton}>
            <Text style={[styles.gradientBtnColor, this.props.smallGradientText, this.props.isFollow]}>{this.props.label}</Text>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    )
  }
}