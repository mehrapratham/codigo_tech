import {Platform, StyleSheet} from 'react-native'

let commonGrayColor = '#f0f3f6';
let primaryColor = '#305983';
let LinkColor = '#4d95f6'
let commonBorderRadius = 3
let commonWhiteColor = '#fff'

export default StyleSheet.create({
  topBox: {
  },
  mainbox: {
    padding: 20
  },
  boxCon: {
    marginBottom: 30
  },
  boxlabel: {
    color: '#898989',
    marginBottom: commonBorderRadius,
    fontSize: 12,
    fontWeight: '300',
    opacity: 0.7
  },
  headertext: {
    color: '#fff'
  },
  headerBtn: {

  },
  hasGrayBackground: {
    backgroundColor: commonGrayColor
  },
  headerBtnIcon: {
    color: commonWhiteColor
  },
  textbox: {
    height: 45,
    backgroundColor: commonGrayColor,
    borderRadius: commonBorderRadius,
    padding: 10,
    borderWidth: 0
  },
  textboxWhitebg: {
    height: 45,
    backgroundColor: commonWhiteColor,
    borderRadius: commonBorderRadius,
    padding: 10,
    borderWidth: 1,
    borderColor: '#d9e1e8'
  },
  forgotLink: {
    marginBottom: 5
  },
  forgotText: {
    fontSize: 12,
    color: '#718daa'
  },
  gradientBtn: {
    height: 45,
    borderRadius: commonBorderRadius,
    alignItems: 'center',
    justifyContent: 'center'
  },
  gradientBtnColor: {
    color: commonWhiteColor,
    letterSpacing: 2,
    fontWeight: 'bold',
    fontSize: 13
  },
  headerBgColor: {
    backgroundColor: 'transparent',
  },
  btmLink: {
    alignItems: 'center',
    padding: 20
  },
  btmLinkText: {
    alignItems: 'center'
  },
  btnLink: {
    color: '#073366',
    fontSize: 12
  },
  btnLinkText: {
    color: LinkColor,
    position: 'relative',
    top: 2,
    fontSize: 12,
  },
  progressBar: {
    height: 7
  },
  progressBarActive: {
    backgroundColor: '#51bcb2', 
    height: 7
  },
  radioBox: {
    backgroundColor: commonWhiteColor,
    borderRadius: commonBorderRadius,
    marginBottom: 15,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  radioBtnText: {
    marginLeft: 10,
    position: 'relative',
    top: -1,
    color: '#a1a2a2',
    fontSize: 14
  },
  marginTop40: {
    marginTop: 40
  },
  marginBottom10: {
    marginBottom: 10
  },
  marginBottom20: {
    marginBottom: 20
  },
  marginBottom30: {
    marginBottom: 30
  },
  marginBottom40: {
    marginBottom: 40
  },
  paddingTop40: {
    paddingTop: 40
  },
  bigHeading: {
    fontSize: 21,
    marginBottom: 25,
    marginTop: 10,
    color: '#073366'
  },
  btmHeading: {
    fontSize: 14,
    color: '#335c85',
    marginBottom: 15
  },
  textWithBullet: {
    fontSize: 11,
    color: '#b2b3b4',
    marginBottom: 15,
    position: 'relative'
  },
  colorBlue: {
    color: primaryColor
  },
  autoCompleteCon: {
    position: 'relative',
    zIndex: 1000,
    flex: 2
  },
  autoCompleteTextbox: {
    backgroundColor: '#fff',
    width: '100%',
    borderRadius: commonBorderRadius,
    borderWidth: 1,
    borderColor: commonGrayColor
  },
  dropDownStyle: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ddd'
  },
  hasFlex: {
    flex: 1
  },
  autoCompleteInput: {
    paddingLeft: 15,
    height: 45
  },
  alignItBottom: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  leftText: {
    position: 'absolute',
    right: 0,
    fontSize: 12,
    color: '#a8a8a8',
    fontWeight: '300'
  },
  checkboxCon: {
    marginTop: -15
  },
  checkboxStyle: {
    borderRadius: commonBorderRadius,
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 5
  },
  checkboxImg: {
    marginLeft: 12,
    marginRight: 12
  },
  textBoxIcon: {
    position: 'absolute',
    bottom: 10,
    right: 15,
    fontSize: 20,
    color: '#bfbfbf'
  },
  checkboxTitle: {
    marginTop: 4
  },
  subTitle: {
    fontSize: 12,
    color: '#bababa',
    marginTop: 5
  },
  selectOpenView: {
    backgroundColor: commonGrayColor,
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 15
  },
  marginBottomZero: {
    marginBottom: 0
  },
  textInputWithoutLabel: {
    height: 40,
    backgroundColor: '#fff',
    paddingLeft: 10,
    borderRadius: 2
  },
  marginBottom15: {
    marginBottom: 15
  },
  fontSize12: {
    fontSize: 12
  },
  leftRightPadding20: {
    paddingLeft: 20,
    paddingRight: 20
  },
  resendCodeText: {
    fontSize: 12,
    color: LinkColor
  },
  otpInputColor: {
    backgroundColor: commonGrayColor,
    borderRadius: commonBorderRadius
  },
  feedCon: {
    backgroundColor: '#fff',
    marginBottom: 15
  },
  feedHeader: {
    padding: 15
  },
  userImg: {
    width: 57
  },
  userInfoCon: {
    position: 'relative'
  },
  userInfo: {
    position: 'absolute',
    top: 0,
    left: 65
  },
  userName: {
    color: primaryColor,
    marginBottom: 3
  },
  userSubTitle: {
    fontSize: 11,
    color: '#6887a4',
    marginBottom: 3
  },
  minAgo: {
    fontSize: 11,
    color: '#a7b9cb'
  },
  rightContainer:{
    position: 'absolute',
    right: 0,
    top: 0
  },
  userConIcons: {
    color: '#bacad8'
  },
  feedImg: {
      width: '100%'
  },
  feedActionCon: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10
  },
  actionsField: {
    flex: 1
  },
  centered: {
    alignItems: 'center'
  },
  rightAligned: {
    alignItems: 'flex-end'
  },
  actionTextCon: {
    color: '#8aa2b8',
    fontSize: 11,
    paddingLeft: 30,
    paddingTop: 15,
    paddingBottom: 15,
    position: 'relative',
  },
  paddingLeft25: {
    paddingLeft: 25
  },
  actionImg: {
    position: 'absolute',
    top: 10
  },
  feedActionIcon: {
    fontSize: 20,
    position: 'relative',
    marginRight: 10
  },
  actionText: {
    fontSize: 12,
    marginLeft: 10,
    borderRadius: commonBorderRadius,
    fontSize: 11
  },
  whiteColor: {
    backgroundColor: commonWhiteColor
  },
  activeFooterTabStyle: {
    backgroundColor: commonWhiteColor,
  },
  FooterTabStyle: {
    backgroundColor: commonWhiteColor
  },
  activeFooterText: {
    color: 'rgba(41,120,235,1)'
  },
  textFooterColor: {
    color: 'rgba(191,214,249,1)'
  },
  fontSize10: {
    fontSize: 9
  },
  notificationText: {
    color: '#4271bf',
    fontSize: 12
  },
  notificationUpperText: {
    color: '#6e8ba8',
    fontSize: 12
  },
  paddingTop0: {
    paddingTop: 0
  },
  thumbnailStyle: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  upperViewNotification: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 15
  },
  topView: {
    paddingTop: 24
  },
  listBorder: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.1)'
  },
  marginTop10: {
    marginTop: 10
  },
  marginRight20: {
    marginRight: 20
  },
  fontSize22: {
    fontSize: 22,
  },
  darkGreyColor: {
    color: '#5b819f',
  },
  marginLeft20: {
    marginLeft: 20
  },
  marginRight10: {
    marginRight: 10
  },
  addGradientButtn: {
    height: 50,
    width: 50,
    borderRadius: 25,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fixedPosition: {
    position: 'absolute',
    bottom: 10,
    right: 10
  },
  colorWhite: {
    color: commonWhiteColor
  },
  boldText: {
    fontWeight: 'bold'
  },
  fontSize26: {
    fontSize: 26,
  },
  lessMargin5: {
    marginTop: -5
  },
  greenColor: {
    color: '#53bda1'
  },
  fontsize13: {
    fontSize: 13
  },
  fontSize16: {
    fontSize: 16
  },
  smallGradientButtn: {
    height: 30,
    width: 106
  },
  marginTop15: {
    marginTop: 15
  },
  smallGradientText: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0
  },
  followGradientText: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0,
    color: '#56bf7d'
  },
  commentBox: {
    paddingLeft: 55,
    paddingRight: 10,
    position: 'relative',
    paddingBottom: 10,
  },
  commentInput: {
    height: 36,
    borderWidth: 1,
    borderColor: '#e3e7eb',
    backgroundColor: '#f9f9f9',
    paddingLeft: 10,
    paddingRight: 65,
    borderRadius: commonBorderRadius
  },
  commentAvatar: {
    position: 'absolute',
    left: 10,
    top: 0
  },
  multiImageCon: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    margin: -1
  },
  multiImgBox: {
    width: '50%',
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: 'red',
    position: 'relative'
  },
  haveMoreImg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(61,125,186,0.5)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  moreText: {
    color: '#fff',
    fontSize: 28
  },
  followCon: {
    position: 'relative',
    padding: 15,
    paddingLeft: 15,
    paddingRight: 15
  },
  mainText: {
    fontSize: 14,
    color: '#003366'
  },
  sideLink: {
    position:'absolute',
    right: 15,
    top: 15
  },
  sideLinkText: {
    color: '#53bea0',
    fontSize: 12
  },
  followCard: {
    borderWidth: 1,
    borderRadius: commonBorderRadius,
    padding: 10,
    width: 150,
    borderColor: '#eeeef3',
    margin: 7.5,
    marginTop: 0,
    alignItems: 'center'
  },
  jobCard: {
    borderWidth: 1,
    borderRadius: commonBorderRadius,
    padding: 10,
    width: 150,
    borderColor: '#eeeef3',
    margin: 7.5,
    marginTop: 0
  },
  followOuterCon: {
    paddingLeft: 7.5,
    paddingRight: 7.5,
    paddingBottom: 20
  },
  followTitle: {
    fontSize: 14,
    color: primaryColor,
    marginBottom: 4,
    marginTop: 5
  },
  followSubTitle: {
    color: '#b1c0d0',
    fontSize: 11,
    marginBottom: 5
  },
  borderBox:{
    width: '100%',
    borderTopWidth: 1,
    borderColor: '#eeeef3',
    height: 1,
    backgroundColor: '#eeeef3',
    marginBottom: 6,
    marginTop: 10
  },
  followLink: {
    color: '#53bea0',
    fontSize: 12,
    marginTop: 3
  },
  followScrollBox: {
    flexDirection: 'row',
    paddingRight: 15
  },
  mutualConText: {
    fontSize: 8,
    color: '#96acbf'
  },
  commentsOuterBox: {
    borderTopWidth: 1,
    borderColor: '#f1f1f1',
    paddingTop: 15,
    paddingBottom: 5,
    marginBottom: -10
  },
  commentRightBox: {
    backgroundColor: commonGrayColor,
    borderRadius: commonBorderRadius,
    padding: 10,
    position: 'relative',
    paddingLeft: 15,
    paddingRight: 15
  },
  nameComment: {
    fontSize: 12,
    color: '#3d648b',
    marginBottom: 3,
    fontWeight: 'bold'
  },
  timeComment: {
    position: 'absolute',
    right: 15,
    fontSize: 11,
    color: '#6a88a5',
    top: 12
  },
  subTitleComment: {
    fontSize: 12,
    color: '#476c91'
  },
  commentText: {
    fontSize: 12,
    color: primaryColor,
    marginTop: 10,
    marginBottom: 10
  },
  smallUserCon: {
    borderTopWidth: 1,
    borderColor: commonGrayColor
  },
  nextToName: {
    fontSize: 12,
    color: '#7e98b1',
    marginLeft: 4
  },
  hasPadding15: {
    padding: 15,
    paddingBottom: 0
  },
  fileFeedCon: {
    backgroundColor: commonGrayColor,
    padding: 10,
    borderRadius: commonBorderRadius,
    paddingLeft: 50,
    paddingRight: 35
  },
  fileNameCon: {
  },
  marginTopMinus20: {
    marginTop: -20
  },
  fileName:{
    fontSize: 13,
    marginBottom: 5,
    color: '#6987a3'
  },
  fileSize: {
    fontSize: 12,
    color: '#adbbcb'
  },
  downloadIcon: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  fileIcon: {
    position: 'absolute',
    top: 15,
    left: 15
  },
  paddingTop5: {
    paddingTop: 5
  },
  paddingTop10: {
    paddingTop: 10
  },
  allumniConText: {
    fontSize: 11,
    marginTop: 0,
    marginBottom: 10,
    color: '#9bafc3'
  },
  allumniCon: {
    position: 'relative',
    marginTop: 25,
    paddingLeft: 30
  },
  allmniIcon: {
    position: 'absolute',
    top: -3
  },
  commentIconsCon: {
    position: 'absolute',
    top: 10,
    right: 15,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  commentIconsTextareaCon: {
    top: 'auto',
    bottom: 20
  },
  commentIconBtn2: {
    width: 18,
    height: 18
  },
  commentIconBtn: {
    
  },
  commentIcon: {
    marginLeft: 10,
    marginRight: 5
  },
  postCon: {
    flex: 1,
    paddingTop: 69,
    position: 'relative',
  },
  postHeader: {
    height: 45,
    backgroundColor: '#fff',
    marginTop: 24,
    position: 'absolute',
    top: 0,
    width: '100%',
    padding: 4,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row'
  },
  workingArea: {
    flex: 1,
    backgroundColor: '#fafbfc'
  },
  rightSide: {
    position: 'absolute',
    top: 12,
    right: 15
  },
  closeBtn: {
    fontSize: Platform.OS === 'ios' ? 40 : 25,
    marginTop: Platform.OS === 'ios' ? 0 : 7
  },
  headerLinkBtn: {

  },
  headerLink: {
    color: LinkColor,
    marginTop: 3
  },
  formCon: {
    padding: 20
  },
  formHeading: {
    fontSize: 20,
    color: primaryColor,
    marginBottom: 20
  },
  pickerStyle: {
    width: '100%', 
    height: 45, 
    padding: 0, 
    margin: 0, 
    borderWidth: 0,
  },
  selectArrow: {
    position: 'absolute',
    top: 20,
    right: 15
  },
  pickerText: {
    fontSize: 12,
    color: primaryColor
  },
  subHeading: {
    fontSize: 16,
    marginBottom: -5
  },
  smallTextHeading: {
    fontSize: 14
  },
  addSkills: {
    backgroundColor: '#f5f7f9',
    borderTopWidth: 1,
    borderColor: '#d4dde5'
  },
  bigLengthBtn: {
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 18,
    borderTopWidth: 0.5,
    borderColor: '#d4dde5'
  },
  LinkColor: {
    color: LinkColor
  },
  boldText: {
    fontWeight: 'bold'
  },
  chipsBox: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 8,
    paddingBottom: 0,
    paddingTop: 10,
    borderWidth: 1,
    minHeight: 45,
    borderColor: '#d9e1e8',
    borderRadius: commonBorderRadius,
    marginBottom: 15
  },
  chip: {
    backgroundColor: '#b2c1d1',
    borderRadius: commonBorderRadius,
    padding: 4,
    paddingLeft: 15,
    paddingRight: 15,
    height: Platform.OS === 'ios' ? 26 : 28,
    position: 'relative',
    flexDirection: 'row',
    paddingRight: 30,
    marginRight: 10,
    marginBottom: 5
  },
  chipClose: {
    position: 'absolute',
    height: 26,
    top: 1,
    right: 5,
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: commonBorderRadius,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0
  },
  chipCloseBtn: {
    fontSize: Platform.OS === 'ios' ? 25 : 18,

    marginTop: Platform.OS === 'ios' ? 0 : 5,
    color: '#fff',
    fontWeight: 'bold'
  },
  chipText: {
    color: '#fff'
  },
  chipsCompleteTextbox: {
    height: 26,
    width: '100%'
  },
  chipsautoCompleteInput: {
    backgroundColor: 'blue'
  },
  chipsautoCompleteInput: {
    height: 26,
  },
  chipOuter: {
    position: 'relative',
    zIndex: 1000
  },
  bringToTop: {
    // zIndex: 100000
  },
  chipsdropDownStyle: {
    backgroundColor: 'red'
  },
  addSkillBtnCon: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  lineDivider: {
    width: 1,
    height: 15,
    backgroundColor: '#dce2e9',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 2
  },
  cancelBtnText: {
    color: '#8ca3ba'
  },
  nextToIcon: {
    marginTop: 11,
    paddingLeft: 10
  },
  leftArrow: {
    fontSize: 25,
    marginTop: 7,
    color: '#7dd5c5'
  },
  atricleText: {
    fontSize: 14,
    color: '#003366',
    marginBottom: 10,
    marginTop: 10,
    lineHeight: 23
  },
  imgBox: {
    borderBottomWidth: 1,
    borderColor: '#e6e7e8'
  },
  articleImg: {
    width: '100%',
    marginBottom: 20
  },
  noPadding: {
    paddingLeft: 0,
    paddingRight: 0
  },
  articleActions: {
    borderTopWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: '#e6e7e8'
  },
  writenByCon: {
    paddingTop: 10,
    marginTop: 20,
    borderTopWidth: 1,
    borderColor: '#e6e7e8',
    paddingBottom: 10
  },
  writtenByInfo: {
    paddingRight: 50
  },
  tagsCon: {
    paddingLeft: 20,
    paddingRight: 20,
    borderTopWidth: 1,
    borderColor: '#e6e7e8',
    paddingTop: 10
  },
  onlyTags: {
    padding: 0
  },
  onlyTagsBox: {
    padding: 0,
    paddingTop: 0,
    paddingBottom: 0,
    minHeight: 10,
    borderWidth: 0,
    backgroundColor: 'transparent'
  },
  membersCon: {
    padding: 20,
    borderTopWidth: 1,
    borderColor: '#e6e7e8',
    backgroundColor: '#f5f7f9'
  },
  rightText: {
    position: 'absolute',
    right: 0,
    fontSize: 14
  },
  memberLabel: {
    fontSize: 12,
    color: '#8b8c8c'
  },
  memberCount: {
    fontSize: 12,
    color: '#3668bb',
    paddingRight: 4
  },
  membersImgCon: {
    flexDirection: 'row',
    marginRight: -10,
    marginLeft: -10
  },
  memberImgBox: {
    flex: 1,
    alignItems:'center'
  },
  memberImg: {
    width: '100%',
    height: '100%'
  },
  memberInner: {
    width: 45,
    height: 45,
    borderRadius: 100,
    position: 'relative',
    overflow: 'hidden'
  },
  haveMore: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(61,125,186,0.5)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  whiteText: {
    color: '#fff',
    fontSize: 12
  },
  hasBottomBorder: {
    borderBottomWidth: 1, 
    borderColor: '#e6e7e8'
  },
  replyCon:{
    paddingLeft: 30,
    position: 'relative'
  },
  replyUserImg: {
    backgroundColor: 'red',
    width: 25,
    height: 25,
    left: -30,
    top: 0
  },
  replyImg: {
    width: '100%',
    height: '100%'
  },
  showReplyLink: {
    marginBottom: 10,
    marginLeft: -30
  },
  replyLinkText: {
    fontSize: 12,
    color: '#0645AD'
  },
  replyActionCon: {
    flexDirection: 'row'
  },
  marginRight10: {
    marginRight: 10
  },
  marginRight20: {
    marginRight: 20
  },
  replyCount: {
    fontSize: 12,
    color: '#41678d'
  },
  replyCountCon:{
    flexDirection: 'row',
    marginTop: 16
  },
  divider: {
    width: 1,
    height: 10,
    backgroundColor: '#41678d',
    marginTop: 2,
    marginRight: 5,
    marginLeft: 5
  },
  darkBlueColor: {
    color: '#335c85'
  },
  paddingLeft10: {
    marginLeft: 20
  },
  dotStyle: {
    position: 'absolute',
    height: 4,
    width: 4,
    borderRadius: 2,
    backgroundColor: '#b2b3b4',
    overflow: 'hidden',
    top: 5
  },
  upperDotViewStyle: {
    paddingLeft: 20,
    position: 'relative'
  },
  fontSize14: {
    fontSize: 14
  },
  paddingBottom100: {
    paddingBottom: 85
  },
  lastGradientView: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    padding: 20,
    backgroundColor: commonGrayColor
  },
  notifyHeading: {
    fontSize: 14,
    color: '#073366'
  },
  lightGreyBackground: {
    backgroundColor: '#f5f7f9'
  },
  notifyName: {
    color: '#325b84',
    fontSize: 12
  },
  lightNotifyTextColor: {
    color: '#7b96af',
    fontSize: 12
  },
  marginTop7: {
    marginTop: 7
  },
  chatUpperText: {
    color: '#073366',
    fontSize: 14
  },
  chatText: {
    color: '#bbc8d6',
    fontSize: 11
  },
  chatTimeText: {
    color: '#386abd',
    fontSize: 12
  },
  chatTitle: {
    color: '#073366',
    fontSize: 21
  },
  msgUnseenName: {
    color: '#073366',
    fontSize: 14,
    fontWeight: 'bold'
  },
  msgUnseenText: {
    color: '#093668',
    fontSize: 11
  },
  marginTop3: {
    marginTop: 3
  },
  backGradient: {
    height: 65,
  },
  feedTextbox: {
    height: 45,
    backgroundColor: commonWhiteColor,
    borderRadius: commonBorderRadius,
    borderWidth: 0,
    paddingLeft: 50,
    paddingRight: 50
  },
  feedSearchIcon: {
    position: 'absolute',
    left: 25,
    top: 20,
    fontSize: 26,
    color: '#bacad8'
  },
  feedSerachView: {
    position: 'relative',
    padding: 10
  },
  feedVoiceIcon: {
    position: 'absolute',
    right: 25,
    top: 20,
    fontSize: 25,
    color: '#bacad8'
  },
  userNameText: {
    fontSize: 14,
    color: '#073366'
  },
  userfeedColor: {
    color: '#335c85',
    fontSize: 14
  },
  userfeedLink: {
    color: '#3a6cbd',
    fontSize: 14
  },
  marginTop5: {
    marginTop: 5
  },
  videoText: {
    color: '#325b84',
    fontSize: 14
  },
  activeTabText: {
    color: '#073467',
    fontSize: 12,
    fontWeight: '600'
  },
  tabText: {
    color: '#6887a4',
    fontSize: 12
  },
  tabBarUnderline: {
    backgroundColor: '#54bd96',
    height: 2
  },
  btnWidth: {
    borderWidth: 2
  },
  followButton: {
    height: '100%', 
    width: '100%', 
    backgroundColor: '#fff', 
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabStyle: {

  },
  likePageBackground: {
    backgroundColor: '#e7f5f9'
  },
  tabOuterBox: {
    padding: 15
  },
  hasBorderBottom: {
    borderBottomWidth: 1,
    borderColor: '#ebeff3'
  },
  atricleTabCon: {
    backgroundColor: '#f6f7f8'
  },
  atricleList: {
    backgroundColor: '#fefefe',
    padding: 15,
    borderRadius: commonBorderRadius,
    shadowOffset:{  width: 0,  height: 0,  },
    shadowColor: '#ddd',
    shadowOpacity: 0.7,
    marginBottom: 12
  },
  articleTitle: {
    fontSize: 14,
    color: primaryColor,
    marginBottom: 5
  },
  atricleSubheading: {
    fontSize: 12,
    color: '#bbc8d6'
  },
  atricleActionIcon: {
    position: 'absolute',
    height: 15,
    width: 3,
    right: 20,
    top: 26
  },
  customTabStyle: {
    backgroundColor: '#fff',
    textAlign: 'center'
  },
  hasWhiteBackground: {
    backgroundColor: '#fff'
  },
  searchHeading: {
    padding: 20,
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderColor: '#f1f1f1'
  },
  headingTitle:{
    fontSize: 14,
    color: '#093668'
  },
  clearSearch: {
    position: 'absolute',
    right: 20,
    top: 17
  },
  clearSearchLink: {
    fontSize: 12,
    color: '#54bd94'
  },
  searchList: {
    paddingLeft: 20,
    paddingRight: 20
  },
  searchListCon: {
    paddingLeft: 45,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    paddingRight: 50
  },
  searchImg: {
    position: 'absolute',
    left: 0,
    top: 2,
    width: 30,
    height: 30
  },
  searchText: {
    fontSize: 12,
    color: primaryColor
  },
  searchType: {
    position: 'absolute',
    right: 0,
    fontSize: 12,
    color: '#647f97',
    top: 10
  },
  customGrayTabStyle: {
    backgroundColor: '#e4e5e7'
  },
  arrowBackIcon: {
    color: '#53bda1'
  },
  searchFoundCon: {
    width: '100%',
    flexDirection: 'row',
    padding: 20,
    paddingBottom: 10
  },
  searchFound: {
    fontSize: 12,
    color: '#6483a1'
  },
  searchFoundBold: {
    fontSize: 12,
    color: '#6483a1',
    fontWeight: 'bold'
  },
  addMenu: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255,255,255,0.7)',
    bottom: 55,
    zIndex: 10000
  },
  innerAddMenu: {
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    minHeight: 100,
    paddingLeft: 10,
    paddingRight: 10
  },
  uploadHeadingCon: {
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderColor: '#edf1f4'
  },
  uploadHeading: {
    fontSize: 15
  },
  uploadLink: {
    paddingTop: 15,
    paddingBottom: 15,
    flexDirection: 'row'
  },
  uploadLinkText: {
    fontSize: 15,
    position: 'relative',
    top: 3
  },
  uploadLinkImg: {
    marginRight: 20
  },
  researchOuterView: {
    position: 'relative',
    paddingLeft: 65,
    backgroundColor: commonWhiteColor,
  },
  avatarResearch: {
    position: 'absolute',
    left: 10,
    top:  10,
    height: 45,
    width: 45,
    overflow: 'hidden',
    borderRadius: 25
  },
  crossResearch: {
    position: 'absolute',
    right: 15,
    top:  20
  },
  darkLightColor: {
    color: '#c4d0dc',
    fontSize: 12
  },
  topTitle: {
    fontSize: 16,
    fontWeight: '600',
    color: '#325b84'
  },
  mainViewResearch: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 50,
    borderBottomWidth: 0.5,
    borderColor: '#edf1f4'
  },
  bottomParticipant: {
    position: 'relative',
    bottom: 0
  },
  addParticipantText: {
    fontWeight: 'bold',
    fontSize: 14
  },
  checkboxImgCard: {
    marginLeft: 1,
    marginRight: 1,
    height: 20,
    width: 20
  },
  researchUpperText: {
    fontSize: 14,
    color: '#57799a'
  },
  transOrBorderRadius: {
    backgroundColor: 'transparent',
    borderRadius: 0,
    zIndex: 100
  },
  otpColor: {
    color: primaryColor
  },
  textEditorBtn: {
    position: 'absolute',
    top: -15,
    zIndex: 10,
    right: 15
  },
  textEditorBtnText: {
    color: '#55c1b4'
  },
  flexDirectionRow: {
    flexDirection: 'row'
  },
  normalText:{
    fontSize: 12,
    color: '#aaaaaa',
    marginBottom: 8
  },
  errorCon: {
    flexDirection: 'row'
  },
  errorText: {
    color: 'red',
    fontSize: 10,
    fontWeight: '300',
    letterSpacing: 1
  },
  errorIcon:{
    marginRight: 10
  },
  thankYouCon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  thankYouHeading: {
    fontSize: 22,
    color: '#073366',
    maxWidth: 150,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20
  },
  thankYouSubHeading: {
    fontSize: 12,
    color: '#b1b1b1',
    maxWidth: 200,
    textAlign: 'center'
  },
  infoText: {
    fontSize: 14,
    letterSpacing: 1,
    color: '#0a3769'
  },
  hasWhiteBG: {
    backgroundColor: '#fff'
  },
  eventBanner: {
    width: '100%'
  },
  eventHeading: {
    fontSize: 16,
    marginBottom: 15,
    fontWeight: '500',
    letterSpacing: 1
  },
  eventTime: {
    fontSize: 12,
    color: '#496e92',
    marginBottom: 8
  },
  eventAddress: {
    fontSize: 12,
    color: '#99adc2',
    marginBottom: 15
  },
  eventBtn: {
    marginTop: 10
  },
  darkGray: {
    backgroundColor: '#f0f0f0'
  },
  faqHeading: {
    fontSize: 16,
    fontWeight: '500',
    marginBottom: 20,
    letterSpacing: 1
  },
  tabContainer: {
    backgroundColor: '#fff',
    marginTop: 1,
    padding: 20
  },
  skillMain:{
    paddingTop:10,
    paddingBottom:10,
    paddingLeft:15,
    paddingRight:15,
    backgroundColor:'#fff'
  },
  skillsTextMain:{
    marginBottom:8,
    fontSize:17
  },
  skillText:{
    fontSize:11,
    color:'#36393d'
  },
  SkillsTextOut:{
    flexDirection:'row',
    marginTop:20,
    position:'relative',
    alignItems:'center',
    paddingRight:100,
    marginBottom:30
  },
  skillTouchBtn:{
    height: 30,
    width: 95,
  },
  skillTouchText:{
    fontWeight:'500',
    fontSize: 13,
    color:'#56bf85',
    letterSpacing: 0
  },
  skillCon:{
    marginTop:15,
  },
  lineStyle:{
    borderWidth: 1,
    borderColor:'black',
    borderColor:'#efefef',
  },
  skillBtnOut:{
    position:'absolute',
    right:0
  },
  skillsImgOut:{
    width: 55,
    height: 55,
    borderRadius: 100,
    position: 'relative',
  },
  skillImg:{
    height:'100%',
    width:'100%',
  },
  skillImageOut:{
    marginLeft:5,
    marginRight:5
  },
  stdLeftArrow:{
    fontSize: 20,
    marginTop: 10
  },
  stdDownArrow:{
    fontSize: 20,
    marginTop:3
  },
  stdMainCon:{
    marginTop:15,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    backgroundColor:'#fff'
  },
  stdMain:{
    flex:1,
    position:'relative',
    paddingTop:80
  },
  stydBackImg:{
    width:'100%',
    height:'100%',
  },
  stdPrfImg:{
    width:'100%',
    height:'100%'
  },
  stdImgOut:{
    height:100,
    borderTopLeftRadius:10,
    borderTopRightRadius:10
  },
  stdPrfImgOut:{
    height:80,
    width:80,
    borderRadius:100,
    overflow:'hidden',
    backgroundColor:'transparent',
    alignSelf:'center',
    marginTop:-35,
    position:'relative',
    borderWidth:0
  },
  stdTextCon:{
    alignItems:'center',
    marginTop:15
  },
  stdTextFirst:{
    fontSize:17,
    color:'#102459',
    fontWeight:'400',
    marginBottom:5
  },
  stdTextSecond:{
    fontSize:12,
    color:'#7f7f7f',
    marginTop:7,
    fontWeight:'200',
    textAlign:'center'
  },
  stdLine:{
    marginTop:15,
    borderWidth: 1,
    borderColor:'#efefef',
    marginLeft:15,
    marginRight:15
  },
  stdLineBtm:{
    borderWidth: 0.5,
    borderColor:'#b5b6b8',
  },
  stdTextSecondDes:{
    fontSize:12,
    color:'#7f7f7f',
    fontWeight:'200',
    textAlign:'center'
  },
  stdTextLink:{
    color:'blue'
  },
  stdTestSecondCon:{
    marginTop:30,
    alignItems:'center'
  },
  stdTextFollow:{
    flexDirection:'row',
    marginTop:30,
    justifyContent:'center'
  },
  stdTextSecondLine:{
    fontSize:12,
    color:'#7f7f7f',
    fontWeight:'200',
    marginLeft:10,
    marginRight:10
  },
  stdTextFollowNumber:{
    marginTop:-2,
    marginRight:5
  },
  StdImgIcon:{
    flexDirection:'row',
  },
  stdImgIconOut:{
    height:20,
    width:20,
  },
  stdBtnOut:{
    marginLeft:15,
    marginRight:15,
    marginTop:30
  },
  stdTouchBtn:{
    height:45,
    marginBottom:20
  },
  stdTouchText:{
    fontWeight:'500',
    fontSize: 13,
    letterSpacing:2
  },
  stdSecondCon:{
    marginTop:15,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    backgroundColor:'#fff',
  },
  stdSndConText:{
    fontSize:14,
    color:'#102459',
    fontWeight:'400',
    marginTop:10,
    marginLeft:20
  },
  stdSndConLine:{
    marginTop:10,
    borderWidth: 0.7,
    borderColor:'#e0dede',
    marginLeft:5,
    marginRight:5
  },
  stdInnerMain:{
    padding:20,
    flexDirection:'row',
    margin:-10
  },
  stdInnerCon:{
    width:'50%',
    padding:5,

  },
  stdInnerConBorder1:{
    borderWidth: 1,
    borderColor:'#edeaea',
    padding:10
  },
  stdInnerConBorder2:{
    borderWidth: 1,
    borderColor:'#edeaea', 
    padding:10
  },
  stdInnerArrowRight:{
    fontSize:20,
    position:'absolute',
    right:5,
    color:'#969494'
  },
  stdInnerTextAligne:{
    flexDirection:"row"
  },
  stdInnerNumber:{
    color:'#56bf85',
    fontSize:19,
  },
  stdInnerText:{
    fontSize:13,
    marginTop:5,
    color:'#969494',
    fontWeight:'300'
  },
  stdInnerText2:{
    fontSize:13,
    color:'#969494',
    fontWeight:'300'
  },
  stdThirdCon:{
    marginTop:15,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    backgroundColor:'#fff',
  },
  stdThdConText:{
    marginTop:10,
    marginLeft:20,
    flexDirection:'row'
  },
  stdThdConText1:{
    fontSize:14,
    color:'#102459',
    fontWeight:'400',
    marginRight:15
  },
  stdThdConText2:{
    fontSize:14,
    color:'#969494',
    fontWeight:'400',
  },
  stdThdConLine:{
    marginTop:10,
    borderWidth: 0.7,
    borderColor:'#e0dede',
    marginLeft:5,
    marginRight:5
  },
  stdThdConLine2:{
    marginTop:5,
    borderWidth: 0.7,
    borderColor:'#e0dede',
    marginLeft:5,
    marginRight:5
  },
  stdThirdConText1:{
    fontSize:14,
    color:'#102459',
    fontWeight:'200',
    marginBottom:5
  },
  stdThirdConText2:{
    fontSize:12,
    color:'#969494',
    fontWeight:'200',
    marginBottom:5
  },  
  stdThirdConText3:{
    fontSize:12,
    color:'#969494',
    fontWeight:'200',
  },
  stdTextConInner:{
    padding:10,
    flexDirection:'row'
  },
  stdThirdConImg:{
    height:60,
    width:60,
    position:'absolute',
    borderRadius:100,
    overflow:'hidden'
  },
  stdThirdInnerCon:{
    position:'relative',
    paddingLeft:80
  },
  stdThdConTextLast:{
    fontSize:13,
    color:'#102459',
    fontWeight:'200',
    textAlign:'center',
    marginTop:10,
    marginBottom:10
  },
  stdtabStyle:{
    marginTop:20
  },
  bkMain:{
    marginLeft:20,
    marginRight:5,
    marginTop:25
  },
  stdTabCon:{
    height:50
  },
  stdtabText:{
    color: '#6b89a6',
    fontSize: 12
  },
  stdactiveTabText:{
    color: '#073467',
    fontSize: 12
  },
  bkHeading:{
    color: '#073467',
    fontSize:14
  },
  bkImg1:{
    width:'50%',
    marginTop:10,
    marginBottom:10
  },
  bkImgOut:{
    flexDirection:'row',
    flexWrap:'wrap',
    marginTop:10,
  
  },
  bkTextIcon:{
    marginTop:10,
    height:12,
    width:12,
    color: '#073467',
    marginRight:15
  },
  bkTextHead:{
    marginTop:10,
    fontSize:12,
    color: '#073467',
  },
  bkImgInner:{
    flexDirection:'row',
    flexWrap:'wrap',
    marginRight:15, 
    margin:-4,
    borderRadius:12,
    overflow:'hidden',
  },
  bkInnerImgOut:{
    height:75,
    width:'50%',
    padding:2
  },
  bkIconOut:{
    flexDirection:'row'
  },
  bkfixedPosition:{
    position:'absolute',
    right:10,
    bottom:-10
  },
  paadingLeftRight20: {
    paddingLeft: 20,
    paddingRight: 20
  },
  textCenter: {
    textAlign: 'center'
  },
  fontSize20: {
    fontSize: 20
  },
  letterSpacing1: {
    letterSpacing: 1
  },
  congratulationText: {
    color: '#53bda1',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
    marginBottom: 15
  },
  blueColorOrfontSize11: {
    fontSize: 11,
    color: '#6b89a6',
    textAlign: 'center'
  },
  marginBottomWidth1: {
    borderBottomWidth: 1,
    borderColor: '#efefef'
  },
  rowDirection: {
    flexDirection: 'row'
  },
  width50: {
    width: '50%'
  },
  greyColorFontSize12: {
    fontSize: 12,
    color: '#7e7e7e',
    letterSpacing: 1
  },
  darkGreyfontSize12: {
    fontSize: 12,
    color: '#676767',
    fontWeight: 'bold'
  },
  personRelativeView: {
    flexDirection: 'row',
    position: 'relative',
    paddingLeft: 70,
    marginTop: 10,
    alignItems: 'center',
    height: 60
  },
  personAbsoluteView: {
    width: 60,
    height: 60,
    borderRadius: 30,
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
    left: 0
  },
  nikhilText: {
    color: '#676767',
    fontSize: 18,
    fontWeight: '500',
  },
  darkGreyfontSize16: {
    fontSize: 16,
    color: '#676767',
    fontWeight: '500'
  },
  timeSize: {
    color: '#696969',
    fontSize: 12,
    marginBottom: 5
  },
  colorDarkgrey: {
    color: '#696969',
    fontWeight: '600',
    fontSize: 12
  },
  fontSize11Dark: {
    color: '#696969',
    fontSize: 12,
    fontWeight: 'bold',
    lineHeight: 18
  },
  accordionHeaderStyle: {
    backgroundColor: '#f4f6f8', 
    padding: 12, 
    fontSize: 10, 
    color: '#486d90', 
    marginBottom: 10, 
    borderRadius: 5, 
    borderWidth: 1, 
    borderColor: '#e4e9ee'
  },
  accordionContentStyle: {
    marginTop: -15, 
    marginBottom: 10, 
    backgroundColor: '#f4f6f8', 
    fontSize: 12, 
    paddingLeft: 18, 
    paddingRight: 18, 
    paddingBottom: 15, 
    color: '#b0bfd0', 
    borderColor: '#e4e9ee', 
    borderRadius: 5, 
    borderWidth: 1,
    borderTopColor: '#f4f6f8',
    position: 'relative',
    zIndex: 100
  },
  expandedIconStyle: {
    color: 'red',
    fontSize: 30
  },
  discissionCon: {
    paddingBottom: 10,
    paddingTop: 20,
    backgroundColor: '#fff',
    marginBottom: 10
  },
  commentBoxTextArea: {
    backgroundColor: '#fff',
  },
  commentTextArea: {
    height: 65,
    paddingRight: 60
  },
  aboutHeading: {
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 10,
    lineHeight: 20
  },
  aboutContent: {
    fontSize: 12,
    color: '#99adc2',
    lineHeight: 18,
    marginBottom: 10
  },
  showMore:{
    fontSize: 12,
    color: '#1954b3'
  },
  eventTagsCon: {
    flexDirection: 'row',
    flexWrap: 'wrap',

  },
  eventTag:{
    backgroundColor: '#e5ecf3',
    padding: 7,
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: commonBorderRadius,
    fontSize: 12,
    color: '#446a8f',
    marginBottom: 10,
    marginRight: 10
  },
  tabOuterBoxEventList: {
    backgroundColor: '#fff'
  },
  mainEventView: {
    borderWidth: 1,
    borderColor: '#f7f7f7',
    position: 'relative',
    paddingLeft: 115,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,

  },
  absoluteViewEvent: {
    position: 'absolute',
    height: 90,
    width: 90,
    borderRadius: 10,
    top: 20,
    left: 10,
    overflow: 'hidden'
  },
  relativeViewEvent: {
    position: 'relative',
    flexDirection: 'row',
    paddingRight: 25,
    marginBottom: 7
  },
  eventManagement: {
    fontSize: 13,
    fontWeight: '600',
    color: '#073366'
  },
  forwardIcon: {
    color: '#56bf83',
    fontSize: 20
  },
  viewAbsoluteEvent: {
    position: 'absolute',
    right: 0,
    top: -2
  },
  fontSize12Grey: {
    fontSize: 12,
    color: '#99adc2'
  },
  marginBottom7: {
    marginBottom: 7
  },
  fontSize12DarkGrey: {
    fontSize: 12,
    color: '#335c85'
  },
  collegeTitle: {
    fontSize: 14,
    fontWeight: '600',
    color: '#325b84',

  },
  heightWidth20: {
    height: 25,
    width: 25,
    marginTop: -5
  },
  marginRight5: {
    marginRight: 5
  },
  outerConCollege: {
    padding: 10,
    borderBottomWidth: 1,
    borderColor: '#f7f7f7',
    height: 70
  },
  relativeConCollege: {
    position: 'relative',
    paddingLeft: 60,
    paddingRight: 75,
    flexDirection: 'row',
  },
  absoluteInnerConCollege: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 50,
    width: 50,
    borderRadius: 25,
    overflow: 'hidden'
  },
  absoluteInnerSecondConCollege: {
    position: 'absolute',
    right: 0,
    top: 5,
  },
  followGradientBtn: {
    height: 35,
    width: 75,
    borderRadius: 4
  },
  followGradientText: {
    color: '#fff',
    letterSpacing: 0,
    fontWeight: '600',
    fontSize: 12
  },
  innerViewCollg: {
    paddingTop: 5,
  },
  maniiPalText: {
    color: '#073366',
    fontWeight: '600',
    fontSize: 14,
    marginBottom: 7
  },
  manipalText2: {
    color: '#7390ab',
    fontSize: 12
  },
  textInput: {
    height: 45,
    backgroundColor: '#f0f3f6',
    borderRadius: 4,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 15
  },
  marginTop15: {
    marginTop: 15
  },
  creditCon: {
    height: 25,
    width: 25,
    position: 'absolute',
    top: 10,
    right: 15
  },
  relativeConView: {
    position: 'relative'
  },
  paddingRight7: {
    paddingRight: 7
  },
  paddingLeft7: {
    paddingLeft: 7
  },
  pickerTextCard: {
    color: '#a7b8ca',
  },
  pickerStyleCard: {
    width: '100%', 
    height: 45, 
    padding: 0, 
    margin: 0, 
    borderWidth: 0,
    backgroundColor: '#f0f3f6'
  },
  checkboxStyleCard: {
    borderRadius: commonBorderRadius,
    backgroundColor: '#fff',
    marginBottom: 15
  },
  rightTextCheckbox: {
    fontSize: 12.1,
    fontWeight: '600',
    color: '#073366',
    borderWidth: 1
  },
  manipalTexttext: {
    marginLeft: 50
  },
  manipalText11: {
    fontSize: 11,
    color: '#7390ab',
    marginBottom: 10
  },
  manipalText113: {
    fontSize: 12,
    color: '#5bc48d',
    fontWeight: '600'
  },
  lastBtnGdt: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    padding: 20
  },
  speakersCon: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  speakerMember: {
    width: '33.33%',
    alignItems: 'center',
    marginBottom: 20
  },
  speakerName: {
    maxWidth: 50,
    color: '#073366',
    textAlign: 'center',
    marginTop: 10
  },
  mapViewCon: {
    borderRadius: 5,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#979797'
  },
  mapView: {
    height: 200
  },
  mediaImgCon: {
    flexDirection: 'row'
  },
  mediaImg: {
    marginRight: 10,
    backgroundColor: 'blue'
  },
  ImgFullWidth: {
    width: 142
  },
  accordionHeader: {
    backgroundColor: '#f4f6f8',
    borderWidth: 1,
    borderColor: '#e4e9ee',
    padding: 15,
    marginBottom: 10,
    borderRadius: 5
  },
  accordionHeaderText: {
    color: '#486d90',
    fontSize: 14
  },
  accordionBody: {
    backgroundColor: '#f4f6f8',
    padding: 15,
    paddingTop: 0,
    borderWidth: 1,
    borderColor: '#e4e9ee',
    borderTopWidth: 0,
    marginBottom: 10,
    marginTop: -12,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5
  },
  accordionBodyText:{
    fontSize: 12,
    color: '#96aabf'
  },
  accordionHeaderIcon:{
    position: 'absolute',
    top: 15,
    right: 15,
    fontSize: 18,
    color: '#53bda2'
  },

  eventFormCon: {
    flex: 1
  },
  formButton: {
    padding: 20,
    width: '100%',
    backgroundColor: '#fff'
  },
  eventHeader: {
    paddingBottom: 0,
    paddingTop: 15,
    marginTop: 24,
    backgroundColor: '#fff',
    paddingRight: 0,
    paddingLeft: 0
  },
  eventHeaderText: {
    fontSize: 16,
    color: '#325b84',
    marginBottom: 10,
    fontWeight: '500',
    marginLeft: 20
  },
  eventStepBtn: {
    borderWidth: 1,
    borderColor: '#dae4ee',
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 4,
    marginRight: 10,
    overflow: 'hidden'
  },
  eventStepsCon: {
    flexDirection: 'row',
    marginTop: 5,
    paddingLeft: 20
  },
  eventStepBtnText: {
    fontSize: 12,
    color: '#4c7093'
  },
  currentStep: {
    borderColor: '#8b9cf3'
  },
  currentStepText: {
    color: '#8b9cf3'
  },
  activeStep: {
    backgroundColor: '#889ff3'
  },
  activeStepText: {
    color: '#fff'
  },
  eventProcessCon: {
    marginBottom: 0,
    marginTop: 15,
    height: 4
  },
  eventProgressBar: {
    height: 4
  },
  eventFormWorkArea: {
    backgroundColor: '#f6f7f9',
    flex: 1
  },
  formWorkArea: {
    paddingTop: 20,
    paddingRight:20,
    paddingLeft:20,
    backgroundColor: '#fff'
  },
  formLabel: {
    fontSize: 13,
    color: '#073366',
    marginBottom: 10,
    fontWeight: '500'
  },
  formInfoText: {
    fontSize: 12,
    color: '#a0b4c7'
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderColor: '#f1f1f1'
  },
  eventTagCon: {
    flexDirection: 'row',

  },
  eventTagText: {
    fontSize: 12,
    color: '#446a8f',
    paddingRight: 20
  },
  tagClose: {
    position: 'absolute',
    right: 10,
    top: 3
  },
  closeBtnTag: {
    fontSize: 25
  },
  alignRight: {
    alignItems: 'flex-end'
  },
  addLink: {
    fontSize: 12,
    color: '#52beba',
    fontWeight: '500'
  },
  headingWithBorder: {
    backgroundColor: '#fff',
    padding: 20,
    paddingTop: 14,
    paddingBottom: 14,
    borderBottomWidth: 1,
    borderColor: '#f1f1f1'
  },
  headingWithBorderText: {
    color: '#073366'
  },
  headingArrow: {
    position: 'absolute',
    right: 20,
    top: 13
  },
  noBorder: {
    borderBottomWidth: 0,
    paddingBottom: 5
  },
  twoColumnView: {
    flexDirection: 'row'
  },
  firstView: {
    flex: 1
  },
  requiredTextSign: {
    color: '#ee596a'
  },
  amMain:{
    flex:1
  },
  amCon:{
    backgroundColor: '#f6f7f9',
    flex: 1
  },
  amTextMain:{
    paddingTop:15,
    paddingLeft:15,
    paddingRight:15,
    fontSize:12,
    fontWeight:'300',
    color:'#5d7993'
  },
  amPhotosOut:{
    backgroundColor:'#fff',
  },
  amPhotoTextOut:{
    padding:15,
    flexDirection:'row',
    alignItems:'center',
  },
  amPhotoText:{
    color:'#102459',
    fontSize:15,
    fontWeight:'500',
    width:'50%'
  },
  amDownArrow:{
    color:'#56bf85',
  },
  amDownArrowOut:{
    width:'50%',
    alignItems:'flex-end'
  },
  amLine:{
    borderWidth: 1,
    borderColor:'#efefef',
  },
  amTouchOut:{
  },
  amTouch:{
    borderWidth: 1,
    borderColor:'#efefef',
    alignItems:'center',
    justifyContent:'center',
    height:45
  },
  amTouchTextBtn:{
    color:'#56bf85',
    fontSize: 12
  },
  amBtn1:{
    marginTop:15
  },
  noMarginTop: {
    marginTop: 0
  },
  amBtnLast:{
    position:'absolute',
    bottom:0,
    width: "100%",
  },
  amBtnOut:{
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    paddingRight:30,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff'
  },
  amTouchBtn:{
    height: 40,
    width:290,
  },
  Btn:{
    fontWeight:'500',
    fontSize: 13,
    color:'#fff',
    letterSpacing: 0
  },
  checkboxheading: {
    color: '#0d386a',
    marginBottom: 20
  },
  checkboxStyleGreen: {
    color: 'green',
    marginBottom: 15
  },
  greenColor: {
    color: '#54c1ae',
    fontSize: 12
  },
  checkboxImgGreen: {
    marginRight: 5
  },
  eventQuestionsCon: {
    backgroundColor: '#fff',
    padding: 20
  },
  hasWhiteBgAcc: {
    backgroundColor: '#fff'
  },
  hasWhiteBgAccBody: {
    backgroundColor: '#fff',
    borderTopWidth: 1,
    paddingTop: 15
  },
  accordionWhiteBodyText: {
    fontSize: 13,
    color: '#365e87'
  },
  amImgContainer:{
    marginTop:10,
    flexDirection:'row',
    flexWrap:'wrap',
  },
  amImgOut:{
    height:60,
    width:'20%',
    padding:2
  },
  amEditTExt:{
    height:50,
    paddingRight:20,
    backgroundColor:'#fff',
    borderTopWidth:1,
    borderColor: '#f1f1f1',
    justifyContent:'center',
  },
  amEditTextOut:{
    justifyContent:'flex-end',
    flexDirection:'row'
  },
  amEditTextMain:{
    marginTop:4,
    fontSize:13 ,
    fontWeight:'300',
    color:'#5d7993'
  },
  dashMain:{
    marginTop:15, 
  },
  dashCon:{
    marginTop:15,
    padding:15,
    backgroundColor:'#fdfdfe',
    paddingBottom:0
  },
  dashTitleMain:{
    flexDirection:'row'
  },
  dashTitleDots:{
    alignItems:'flex-end',
    marginTop:10,
    width:'15%',
  },
  dashText:{
    fontSize:12,
    marginLeft:15,
    color:'#5d7993'
  },
  dashTextMain:{
    fontSize:17,
    color:'#073366',
    marginTop:5,
    width:'85%'
  },
  dashTextTime:{
    fontSize:12,
    marginTop:15,
    color:'#5d7993'
  },
  dashConMain:{
    marginTop:15,
    backgroundColor:'#fff'
  },
  dashTextAligne:{
    flexDirection:"row",
    alignItems:'center',
    borderWidth:0.7,
    borderColor:'#e5eaef',
    padding:15
  },
  dashTextTitle:{
    fontSize:14,
    color:'#073366',
    width:'80%',
    fontWeight:'500'
  },
  dashTextAll:{ 
    fontSize:13,
    width:'20%',
    alignSelf:"flex-end",
    color:'#56bf85',
  },
  dashConBar:{
    borderWidth:0.7,
    borderColor:'#e5eaef',
    borderTopWidth:0,
    borderBottomWidth:0
  },
  dashInnerTest:{
    fontSize:13,
    fontWeight:'300',
    color:'#5d7993',
    marginLeft:15
  },
  dashRadio:{
     color:'#56bf85',
  },
  dashBox:{
    flexDirection:'row',
  },
  dashInnerBox:{
    paddingTop:15,
    paddingBottom:15,
    width:'50%',
    borderWidth:0.7,
    borderColor:'#e5eaef',
    justifyContent:'center',
    alignItems:'center',
  },
  noRightBorder: {
    borderRightWidth: 0
  },
  dashBoxText:{
    color:'#5d7993',
    marginBottom:5
  },
  dashBoxInnerNumber:{
    color:'#073366',
    fontSize:18,
    marginTop:5
  },
  dashBtnOut:{
    padding:15,
    width:'100%',
    borderWidth:0.7,
    borderColor:'#e5eaef',
    justifyContent:'flex-end',
    alignItems:'flex-end',
    borderTopWidth:0
  },
  dashBtnText:{
    color: commonWhiteColor,
    letterSpacing: 0,
    fontWeight: 'bold',
    fontSize: 12,
    marginLeft:15,
    marginRight:15
  },
  dashInviteBox:{  
    borderWidth:0.7,
    borderColor:'#e5eaef',
    margin:15,
    backgroundColor:'#fff',
    padding:15,
  },
  dashInviteTextmember:{
    color:'#073366',
    fontSize:17,
  },
  dashInviteText:{
    color:'#5d7993',
    fontSize:12,
    fontWeight: '300'
  },
  DashFormButton: {
    padding: 20,
    width: '100%',
    backgroundColor: '#fff',
  },
  dashGradientBtn: {
    height: 7,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dashProgressBarActive: {
    marginTop:25,
    marginBottom:10,
    backgroundColor: '#d6d9e4', 
    height: 7,
    borderRadius:10,
    overflow: 'hidden'
  },
  dashBoxIcon:{
    fontSize:14,
    color:'#56bf85',
    marginLeft:15,
    marginTop:5
  },
  dashBoxtextOut:{
    marginTop:10,
    flexDirection:'row'
  },
  dashBarTitleOut:{
    flexDirection:'row',
    margin:15
  },
  dashBarTitle:{
    color:'#073366',
    fontSize:14,
  },
  dashBarDetails:{
    color:'#5d7993',
    fontSize:12,
    marginLeft:5
  },
  paddinBottom20: {
    paddingBottom: 20
  },
  webRelative: {
    position: 'relative',
    paddingRight: 30,
    flexDirection: 'row',
  },
  webAbsolute: {
    position: 'absolute',
    right: 0,
    top: 0,
    height: 20,
    width: 20
  },
  workshopText: {
    fontSize: 17,
    fontWeight: '600',
    color: '#325b84'
  },
  eventCreateText: {
    fontSize: 12,
    color: '#97abc1'
  },
  eventDateText: {
    color: '#7691ac',
    fontSize: 12,
    fontWeight: '500'
  },
  marginBottom5: {
    marginBottom: 5
  },
  freeEventText: {
    fontSize: 13,
    color: '#86a1f3',
    fontWeight: '700'
  },
  aboutEventContent: {
    fontSize: 11,
    color: '#577899',
    lineHeight: 15,
  },
  showMoreEvent: {
    color: '#65c988',
    fontSize: 11
  },
  borderBottomWidth1: {
    borderBottomWidth: 0.5,
    borderColor: '#f1f1f1'
  },
  darkTextEvent: {
    color: '#103762',
    fontSize: 13,
    lineHeight: 19,
    fontWeight: '500'
  },
  relativeViewEvents: {
    position: 'relative',
    paddingLeft: 60,
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
  },
  absoluteViewEvents: {
    position: 'absolute',
    height: 50,
    width: 50,
    borderRadius: 25,
    overflow: 'hidden'
  },
  marginBottom3: {
    marginBottom: 3
  },
  relativeConEvent: {
    position: 'relative',
    paddingLeft: 20,
    flexDirection: 'row'
  },
  absoluteConEvent: {
    height: 15,
    width: 15,
    top: 0,
    left: 0,
    position: 'absolute'
  },
  mapViewEvent: {
    height: 150
  },
  outerMapViewEvent: {
    paddingBottom: 10,
    backgroundColor: '#fff',
    marginBottom: 15
  },
  eventSkillCon: {
    marginBottom: 15,
    borderRadius: commonBorderRadius,
    borderWidth: 0.5,
    borderColor: '#e6ebee',
    overflow: 'hidden',
    padding: 10
  },
  eventSkillMain:{
    padding: 15,
    backgroundColor:'#fff'
  },
  alignItemEnd: {
    alignItems: 'flex-end'
  },
  eventSkillsTextMain: {
    fontSize: 14,
    fontWeight: '500',
    color: '#123964'
  },
  borderPaddingEvent: {
    borderBottomWidth: 0.5,
    paddingTop: 5,
    paddingBottom: 15,
    borderColor: '#edf1f3',
    marginBottom: 10
  },
  textColor25: {
    color: '#4977bb',
    fontSize: 12,
    fontWeight: '600'
  },
  eventMembersText: {
    fontSize: 12,
    color: '#858585'
  },
  paddingBottomTop15: {
    paddingBottom: 15,
    paddingTop: 7
  },
  eventSkillsView: {
    paddingTop: 20,
    paddingBottom: 0
  },
  eventSkillsTextOut: {
    position: 'relative',
    flexDirection: 'row',
    paddingLeft: 20,
    
  },
  eventSkillsTextIn: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 15,
    height: 15
  },
  spotsEventText: {
    color: '#7a92aa',
    fontSize: 12
  },
  textColor15: {
    fontSize: 13,
    color: '#244b72',
    fontWeight: '600'
  },
  accordionEventHeader: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#e4e9ee',
    padding: 15,
    marginBottom: 10,
    borderRadius: 5
  },
  accordionEventBody: {
    backgroundColor: '#fff',
    padding: 15,
    borderWidth: 1,
    borderColor: '#e4e9ee',
    marginBottom: 10,
    marginTop: -12,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5
  },
  reqstView: {
    padding: 20,
    backgroundColor: '#f2f3f5'
  },
  wireImgView: {
    width: 20,
    height: 20,
    position: 'absolute',
    top: 0,
    right: 0
  },
  relativeWireCon: {
    position: 'relative',
    paddingRight: 25
  },
  reviewRelativeCon: {
    position: 'relative',
    paddingLeft: 50,
    paddingRight: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  absoluteConWire: {
    height: 40,
    width: 40,
    overflow: 'hidden',
    borderRadius: 20,
    position: 'absolute',
    top: 0,
    left: 0
  },
  relConList: {
    position: 'relative',
    paddingRight: 100,

  },
  absConList: {
    position: 'absolute',
    top: 0,
    right: 0
  },
  absoluteWireCon: {
    position: 'absolute',
    height: 15,
    width: 15,
    top: 0,
    right: 0
  },
  lightColor: {
    color: '#9fb0c2',
    fontSize: 11
  },
  darkColor: {
    fontSize: 11,
    color: '#3b5f82',
    fontWeight: '600'
  },
  smallGradientButtnReview: {
    height: 35,
    width: 90
  },
  smallGradientTextReview: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0,
    color: '#6db9a3'
  },
  mainName: {
    color: '#3a5e81',
    fontSize: 14,
    fontWeight: '500'
  },
  alignCenter: {
    alignItems: 'center'
  },
  color40: {
    color: '#65819c',
    fontSize: 11,
    fontWeight: '600'
  },
  darkColor2: {
    fontSize: 11,
    color: '#3b5f82',
  },
  barChartCon: {
    borderBottomWidth: 1,
    borderColor: '#e5eaef',
    marginBottom: 20
  },
  mainHeading: {
    height: 100,
    marginTop: 24,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainHeadingtext: {
    color: '#4a4a4a',
    fontSize: 18
  },
  blockBtnText: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  blockBtn: {
    borderRadius: 3,
    backgroundColor: '#2a87e0'
  },
  borderBottomWidth2: {
    borderBottomWidth: 1,
    borderColor: '#f1f1f1'
  },
  splashCon: {
    flex: 1,
    backgroundColor: 'red'
  },
  splashLogo: {
    fontSize: 40
  }
});