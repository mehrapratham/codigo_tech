import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'blue'
	},
	firstCon: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		paddingLeft: 70,
		paddingRight: 70
	},
	imgCon: {
		height: 150,
		width: 150
	},
	img: {
		width: '100%',
		height: '100%'
	},
	thirdCon:{
		flex: 1,
		justifyContent: 'center'
	},
	secondCon: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	textStyle: {
		fontSize: 22,
		color: '#fff',
		textAlign: 'center'
	},
	lastView: {
		flexDirection: 'row',
		justifyContent: 'center'
	},
	dotStyle: {
		height: 10,
		width: 10,
		borderRadius: 5,
		marginRight: 10
	},
	seconLastView: {
		marginTop: 25,
		alignItems: 'center'
	},
	thirdLastView: {
		marginTop: 30,
		alignItems: 'center',
		paddingRight: 20,
		paddingLeft: 20
	},
	loginText: {
		color: '#fff'
	},
	buttn: {
		height: 45,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%'
	}
})